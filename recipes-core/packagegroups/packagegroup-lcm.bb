DESCRIPTION = "Packagegroups for LCM on RDK-B"

inherit packagegroup

RDEPENDS_packagegroup-lcm = " \
    cthulhu \
    timingila \
    timingila-cthulhu \
    ${@bb.utils.contains('DISTRO_FEATURES', 'lcm-images', 'rlyeh timingila-rlyeh image-authentication', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'lcm-bundles', 'celephais timingila-celephais', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'lxc-backend', 'lxc cthulhu-lxc', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'crun-backend', 'crun cthulhu-crun', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'network-plugins', 'cthulhu-networking cthulhu-dhcpc', '', d)} \
    amx-cli \
    mod-ba-cli \
    mod-amxb-rbus \
    mod-sahtrace \
    syslog-ng \
"
