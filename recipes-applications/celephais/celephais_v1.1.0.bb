SUMMARY = "prplLCM application Celephais"
DESCRIPTION = "Celephais is a repository application for OCI bundles for use with the prplLCM"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=2b4b9871fa75f8b15d1c4c7129674b74"

inherit cmake systemd

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/applications/celephais.git;protocol=https;nobranch=1 \
           file://lcm-celephais.service"
SRCREV = "8b985830c948f6ce3a5dc3c6098c11f004e62bed"
S = "${WORKDIR}/git"

DEPENDS = "libamxc libamxp libamxd libamxo libamxm libsahtrace libcelephais libarchive"
RDEPENDS:${PN} += "libamxc libamxp libamxd libamxo libamxm libsahtrace libcelephais libarchive amxrt bash timingila-celephais"

TARGET_CFLAGS += "-I${STAGING_INCDIR}/celephais"

do_install_append () {
     install -d -m 0755 ${D}/usr/bin
     ln -sfr ${D}/usr/bin/amxrt ${D}/usr/bin/celephais

     install -D -m 0644 ${WORKDIR}/lcm-celephais.service ${D}${systemd_unitdir}/system/lcm-celephais.service
     rm -rf ${D}/usr/lib/x86_64-linux-gnu
     rm -r ${D}/usr/lib/
}

SYSTEMD_AUTO_ENABLE = "enable"
SYSTEMD_SERVICE_${PN} = "lcm-celephais.service"

FILES:${PN} += "/etc/amx/celephais/*.odl"
FILES:${PN} += "/etc/amx/celephais/celephais${SOLIBS}"
FILES:${PN} += "/usr/bin/celephais"

# we need the symlink in the base package since this is an ambiorix app. Insane skip needed
INSANE_SKIP_${PN} += "dev-so"
FILES:${PN} += "/etc/amx/celephais/celephais.so"
