FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SUMMARY = "Rlyeh is the local LCM repository"
LICENSE += "SAH & BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit pkgconfig systemd config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/applications/rlyeh.git;protocol=https;nobranch=1 \
           file://lcm-rlyeh.service"
SRCREV = "04bbf938837bcb8c2ebedd4df76db6a53ae3a6b1"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxp libamxd libamxo libamxm liblcm libsahtrace librlyeh"

RDEPENDS:${PN} += "libamxc libamxp libamxd libamxo libamxm libsahtrace amxrt librlyeh \
                   timingila-rlyeh curl (>= 7.8)"

TARGET_LDFLAGS += " -lcurl "

SYSTEMD_SERVICE_${PN} = "lcm-rlyeh.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

do_install_append () {
    install -D -m 0644 ${WORKDIR}/lcm-rlyeh.service ${D}${systemd_unitdir}/system/lcm-rlyeh.service
}

FILES:${PN} += "${systemd_unitdir}/system/lcm-rlyeh.service"
FILES:${PN} += "/etc/amx/rlyeh/*.odl"
FILES:${PN} += "/usr/lib/amx/rlyeh/rlyeh.so"
FILES:${PN} += "/etc/amx/rlyeh/default-policy.json"
FILES:${PN} += "/usr/bin/rlyeh"
