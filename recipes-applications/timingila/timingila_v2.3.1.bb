FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SUMMARY = "High level LCM component"
LICENSE += "BSD-2-Clause-Patent & SAH"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit pkgconfig config-prpl-lcm systemd

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/applications/timingila.git;protocol=https;nobranch=1 \
           file://001-timingila-device-softwaremodules.patch \
           file://lcm-timingila.service"
SRCREV = "bc97431bde0832a317eb1f8ba985866aac33dc73"

S = "${WORKDIR}/git"

DEPENDS += "libsahtrace libamxc libamxp libamxd libamxo libamxm liblcm util-linux"

RDEPENDS:${PN} += "libsahtrace libamxc libamxp libamxd libamxm libamxo liblcm mod-dmproxy amxrt \
                   util-linux timingila-cthulhu"

CONFIG_SAH_SERVICES_TIMINGILA_ADAPTER_PACKAGER = "${@bb.utils.contains('DISTRO_FEATURES', 'crun-backend', '/usr/lib/timingila-celephais/timingila-celephais.so', '/usr/lib/timingila-rlyeh/timingila-rlyeh.so', d)}"
EXTRA_OEMAKE += " DEFINE_ALL_BOOLEANS=y CONFIG_SAH_SERVICES_TIMINGILA_ADAPTER_PACKAGER=${CONFIG_SAH_SERVICES_TIMINGILA_ADAPTER_PACKAGER}"

SYSTEMD_SERVICE_${PN} = "lcm-timingila.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

do_install_append () {
    install -D -m 0644 ${WORKDIR}/lcm-timingila.service ${D}${systemd_unitdir}/system/lcm-timingila.service
}

FILES:${PN} += "${systemd_unitdir}/system/lcm-timingila.service"
FILES:${PN} += "/etc/amx/timingila/timingila.odl"
FILES:${PN} += "/etc/amx/timingila/timingila_defaults.odl"
FILES:${PN} += "/etc/amx/timingila/timingila_definition.odl"
FILES:${PN} += "/etc/amx/timingila/softwaremodules_defaults.odl"
FILES:${PN} += "/etc/amx/timingila/softwaremodules_definition.odl"
FILES:${PN} += "/etc/amx/tr181-device/extensions/01_device-softwaremodules_mapping.odl"
FILES:${PN} += "/usr/lib/amx/timingila/timingila.so"
FILES:${PN} += "/usr/bin/timingila"
FILES:${PN}-dev += "${INCLUDEDIR}/timingila/*.h"
