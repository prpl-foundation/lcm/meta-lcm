FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

SUMMARY = "Cthulhu manages the lifecycle of containers"
LICENSE += "SAH & BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit pkgconfig config-prpl-lcm systemd

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/applications/cthulhu.git;protocol=https;nobranch=1"
SRC_URI += "file://001-bundle-support-fixes.patch \
            ${@bb.utils.contains('DISTRO_FEATURES', 'crun-backend', 'file://002-enable-bundle-support-odl.patch', '', d)} \
            file://lcm-cthulhu.service"
SRCREV = "8c317483e2e48c9121c1cd5e0da2936b088675e5"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxp libamxd libamxo libamxm libamxj libamxb libsahtrace \
            yajl libcthulhu liblcm libarchive libocispec libnl"

RDEPENDS:${PN} += "libamxc libamxp libamxd libamxo libamxm libamxj libamxb libsahtrace amxrt \
                   yajl libcthulhu liblcm libarchive libocispec libnl libnl-route \
                   e2fsprogs e2fsprogs-e2fsck e2fsprogs-resize2fs timingila-cthulhu \
                   syslog-ng"

SYSTEMD_SERVICE_${PN} = "lcm-cthulhu.service"
SYSTEMD_AUTO_ENABLE_${PN} = "enable"

do_install_append () {
    install -D -m 0644 ${WORKDIR}/lcm-cthulhu.service ${D}${systemd_unitdir}/system/lcm-cthulhu.service
}

FILES:${PN} += "${systemd_unitdir}/system/lcm-cthulhu.service"
FILES:${PN} += "/etc/amx/cthulhu/*.odl"
FILES:${PN} += "/etc/amx/cthulhu/defaults.d/*.odl"
FILES:${PN} += "/usr/lib/amx/cthulhu/cthulhu.so"
FILES:${PN} += "/etc/amx/cthulhu/syslog-ng-lcm.conf"
FILES:${PN} += "/usr/bin/cthulhu"
