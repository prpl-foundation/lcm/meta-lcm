SUMMARY = "Rlyeh image authentication plugin"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/plugins/rlyeh/image-authentication.git;protocol=https;nobranch=1" 
SRCREV = "0ed10225b7dc107f14bee32bbd6fa81ac4d35292"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxm libamxp libamxd libamxo libamxb libsahtrace libcthulhu liblcm librnp librlyeh"
RDEPENDS:${PN} += "libamxc libamxm libamxp libamxd libamxo libamxb libsahtrace libcthulhu liblcm librnp librlyeh"

do_install:prepend() {
    # This is failing even on prplos for some reason if you build the plugin explicitly.
    # Either missing dependency or a bug in the install. To bypass the missing dir error
    # let's create a stub.
    mkdir -p ${S}/keys
    touch ${S}/keys/keystub
}

INSANE_SKIP:${PN} += "installed-vs-shipped"

FILES:${PN} += "/usr/lib/amx/cthulhu/plugins/cthulhu-networking.so"
FILES:${PN} += "/usr/lib/amx/cthulhu/plugins/mibs/*.odl"
