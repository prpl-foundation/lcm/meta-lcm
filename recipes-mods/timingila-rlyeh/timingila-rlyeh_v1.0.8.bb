SUMMARY = "The module timingila-rlyeh provides rlyeh support as a packager backend to timingila"
LICENSE += "SAH & BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/modules/timingila-rlyeh.git;protocol=https;nobranch=1"
SRCREV = "c28d31a7c10f230d93e721dad63ff76a4da6ca69"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxm libamxp libamxd libamxo libamxb libsahtrace timingila librlyeh"
RDEPENDS:${PN} += "libamxc libamxm libamxp libamxd libamxo libamxb libsahtrace rlyeh timingila"

FILES:${PN} += "${libdir}/timingila-rlyeh/timingila-rlyeh.so"
