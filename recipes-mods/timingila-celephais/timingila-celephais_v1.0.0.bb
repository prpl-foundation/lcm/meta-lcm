SUMMARY = "prplLCM module timingila-celephais"
DESCRIPTION = "timingila-celephais is an adapter module for celephais."
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=2b4b9871fa75f8b15d1c4c7129674b74"

inherit pkgconfig config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/modules/timingila-celephais.git;protocol=https;nobranch=1"
SRCREV = "a795dc3b8793ea69717f067551c69ccc98e69002"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxm libamxp libamxd libamxb libsahtrace libcelephais timingila"

RDEPENDS:${PN} += "libamxc libamxm libamxp libamxd libamxb libsahtrace libcelephais timingila"

do_compile() {
  cd ${S}/src
  make
}

FILES:${PN} += "${libdir}/timingila-celephais/timingila-celephais.so"
