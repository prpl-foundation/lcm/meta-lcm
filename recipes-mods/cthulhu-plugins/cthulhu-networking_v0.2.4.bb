SUMMARY = "Cthulhu networking plugin"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/plugins/cthulhu/cthulhu-networking.git;protocol=https;nobranch=1" 
SRCREV = "137980225c4a61e1e1b86b607016766872cd68e2"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxm libamxp libamxd libamxo libamxb libsahtrace libcthulhu liblcm"

RDEPENDS:${PN} += "libamxc libamxm libamxp libamxd libamxo libamxb libsahtrace libcthulhu liblcm"

FILES:${PN} += "/usr/lib/amx/cthulhu/plugins/cthulhu-networking.so"
FILES:${PN} += "/usr/lib/amx/cthulhu/plugins/mibs/*.odl"
