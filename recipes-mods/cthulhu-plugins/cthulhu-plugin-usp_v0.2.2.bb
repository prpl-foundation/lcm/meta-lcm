SUMMARY = "cthulhu plugin to configure usp on the host"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/plugins/cthulhu/cthulhu-plugin-usp.git;protocol=https;nobranch=1"
SRCREV = "e9ca932b054c978c3fd9f60c8465b7e787712936"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxm libamxp libamxd libamxo libamxb libsahtrace libcthulhu libocispec liblcm util-linux"

RDEPENDS:${PN} += "libamxc libamxm libamxp libamxd libamxo libamxb libsahtrace libcthulhu libocispec liblcm util-linux-libuuid"

FILES_${PN} = " \
    /usr/lib/amx/cthulhu/plugins/cthulhu-plugin-usp.so \
    /etc/amx/cthulhu/extensions/cthulhu-plugin-usp.odl \
    /etc/amx/cthulhu/extensions/cthulhu_usp.odl \
"
