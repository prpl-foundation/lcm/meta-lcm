SUMMARY = "Cthulhu dhcpc plugin"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/plugins/cthulhu/cthulhu-dhcpc.git;protocol=https;nobranch=1" 
SRCREV = "5355fd2eb5ce26b23decd191ea69b1a8b305a2f7"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxm libamxp libamxd libamxo libamxb libsahtrace libcthulhu liblcm"

RDEPENDS:${PN} += "libamxc libamxm libamxp libamxd libamxo libamxb libsahtrace libcthulhu liblcm"

FILES:${PN} += "/usr/lib/amx/cthulhu/plugins/cthulhu-dhcpc.so"
FILES:${PN} += "/usr/lib/amx/cthulhu/plugins/mibs/*.odl"
FILES:${PN} += "/usr/lib/amx/cthulhu/plugins/dhcpc/*.script"
