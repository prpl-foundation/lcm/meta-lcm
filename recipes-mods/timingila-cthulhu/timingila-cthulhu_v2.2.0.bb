SUMMARY = "The module timingila-cthulhu provides cthulhu support as a container backend to timingila"
LICENSE += "SAH & BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/modules/timingila-cthulhu.git;protocol=https;nobranch=1"
SRCREV = "5e99e01b001882e71c5b0bb34a14330d8303ebab"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxm libamxp libamxb libsahtrace libcthulhu timingila"
RDEPENDS:${PN} += "libamxc libamxm libamxp libamxb libsahtrace cthulhu timingila"

FILES:${PN} += "${libdir}/timingila-cthulhu/timingila-cthulhu.so"
