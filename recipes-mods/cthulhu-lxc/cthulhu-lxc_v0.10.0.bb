FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
SUMMARY = "The module Cthulhu-LXC provides LXC support as a backend to Cthluhu"
LICENSE += "SAH & BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit pkgconfig config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/modules/cthulhu-lxc.git;protocol=https;nobranch=1 \
           file://001-cthulhu-lxc-disable-autodev.patch"
SRCREV = "55a04f7ebbcae3d3acb55cc972f7e14037d4507e"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxm libamxj libamxp libamxd libamxo libsahtrace libcthulhu \
            liblcm yajl lxc"

RDEPENDS:${PN} += "libamxc libamxm libamxj libamxp libamxd libamxo libsahtrace libcthulhu \
                   liblcm yajl lxc"

FILES:${PN} += "${libdir}/cthulhu-lxc/cthulhu-lxc.so"
FILES:${PN} += "/etc/amx/cthulhu-lxc/default.conf"
FILES:${PN} += "/etc/amx/cthulhu-lxc/common.seccomp"
