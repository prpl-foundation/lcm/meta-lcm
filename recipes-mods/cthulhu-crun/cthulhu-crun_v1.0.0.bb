SRC_URI = "git://gitlab.com/prpl-foundation/lcm/modules/cthulhu-crun.git;protocol=https;nobranch=1"
SRCREV = "ace3e2414457dc6ebdbebe7630abb2751839ba43"
S = "${WORKDIR}/git"

inherit config-prpl-lcm

SUMMARY = "Cthulhu-crun module"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE;md5=2b4b9871fa75f8b15d1c4c7129674b74"

DEPENDS += "yajl libamxc libamxm libamxj cthulhu crun libsahtrace"
RDEPENDS_${PN} += "yajl libamxc libamxm libamxj cthulhu crun libsahtrace"

EXTRA_OEMAKE += " STAGINGDIR=${STAGING_DIR_HOST} "

do_install_append() {
    install -d ${D}${libdir}/cthulhu-crun
    install -d ${D}/etc/amx/cthulhu-crun
    install -m 644 ${S}/output/${TARGET_SYS}/cthulhu-crun.so ${D}${libdir}/cthulhu-crun
    install -m 644 ${S}/config/default.json ${D}/etc/amx/cthulhu-crun
}

