# Copied from meta-amx here to remove dependency on it in case something changes.
# These dirs are relevant to currently used sysroot, so although it might look like
# they point to hosts directories - they don't.

EXTRA_OEMAKE += "DEST=${D} \
                 PREFIX=${prefix} \
                 LIBDIR=${libdir} \
                 BINDIR=${bindir} \
                 INCLUDEDIR=${includedir} \
                 "

do_install() {
    oe_runmake install
}

