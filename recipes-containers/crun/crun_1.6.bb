DESCRIPTION = "A fast and low-memory footprint OCI Container Runtime fully written in C."
LICENSE = "LGPLv2.1 & GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=b234ee4d69f5fce4486a80fdaf4a4263 \
                    file://COPYING.libcrun;md5=4fbd65380cdd255951079008b364516c"

inherit pkgconfig autotools-brokensep

SRC_URI = "gitsm://github.com/containers/crun;branch=main \
           file://0001-Add-libcrun-get-version.patch"
SRCREV = "18cf2efbb8feb2b2f20e316520e0fd0b6c41ef4d"
S = "${WORKDIR}/git"

DEPENDS += "yajl libcap libseccomp systemd"
RDEPENDS:${PN} += "yajl libcap libseccomp systemd"

EXTRA_OECONF += "--enable-shared"

do_configure_prepend() {
    ./autogen.sh
}

do_install_append() {
    install -d ${D}/usr/include/crun/libocispec

    install -D ${S}/config.h ${D}/usr/include/crun/
    install -D ${S}/src/libcrun/container.h ${D}/usr/include/crun/
    install -D ${S}/src/libcrun/error.h ${D}/usr/include/crun/
    install -D ${S}/src/libcrun/status.h ${D}/usr/include/crun/

    install -D ${S}/libocispec/src/json_common.h ${D}/usr/include/crun/libocispec/
    install -D ${S}/libocispec/src/runtime_spec_schema_config*.h ${D}/usr/include/crun/libocispec/
    install -D ${S}/libocispec/src/runtime_spec_schema_def*.h ${D}/usr/include/crun/libocispec/
}
