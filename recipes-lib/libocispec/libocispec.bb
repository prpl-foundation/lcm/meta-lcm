SUMMARY = "OCI image and runtime parser"
DESCRIPTION = "A library for easily parsing of OCI runtime and OCI image files from C, and generate json string from corresponding struct"
LICENSE += ""
LIC_FILES_CHKSUM += "file://COPYING;md5=4b1c5af6ba5a6cb36708123c05503f3a"

inherit pkgconfig autotools

SRC_URI = "git://github.com/containers/libocispec.git;protocol=http;nobranch=1 \
           file://001-shared_object.patch \
           https://github.com/opencontainers/image-spec/archive/refs/tags/v1.0.1.zip;name=image-spec \
           https://github.com/opencontainers/runtime-spec/archive/refs/tags/v1.0.2.zip;name=runtime-spec "
SRCREV = "46b870958a39547026c5dc9a044951e232ecbd9c"

SRC_URI[image-spec.sha256sum] = "b60fba7fe7b81a92d5fa2bf60ff9523e9024ecb67a899fb822e36c0392a5b7aa"
SRC_URI[runtime-spec.sha256sum] = "8b941f5458f63ee179cd226176310c04864e656fb708d23151a05f7e80513f26"
S = "${WORKDIR}/git"

COMPONENT = "libocispec"

DEPENDS += "yajl"
RDEPENDS:${PN} += "yajl"

do_configure:prepend() {
  rm -rf ${S}/image-spec ${S}/runtime-spec
  cp -a ../image-spec-1.0.1 ${S}/image-spec
  cp -a ../runtime-spec-1.0.2 ${S}/runtime-spec
}

FILES_SOLIBSDEV = ""
FILES:${PN} += "/usr/lib/libocispec.so"

INSANE_SKIP:${PN} += "src-uri-bad"
INSANE_SKIP:${PN} += "dev-so"
INSANE_SKIP:${PN} += "dev-elf"
