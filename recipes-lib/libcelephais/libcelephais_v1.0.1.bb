FILESEXTRAPATHS:prepend := "${THISDIR}/files:"
SUMMARY = "Support library for Celephais packager"
DESCRIPTION = "Contains functions, defines, variables etc. used by the OCI bundle packager Celephais"
LICENSE += "BSD-2-Clause-Patent"
LIC_FILES_CHKSUM = "file://LICENSE;md5=2b4b9871fa75f8b15d1c4c7129674b74"

inherit pkgconfig config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/libraries/libcelephais.git;protocol=https;nobranch=1"
SRCREV = "0e1346781983a3bbfd5c58ab2e7838a33472e3c4"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxj libsahtrace curl openssl yajl gpgme"
RDEPENDS:${PN} += "libamxc libamxj libsahtrace curl openssl yajl gpgme"

FILES:${PN} += "/usr/lib/libcelephais${SOLIBS}"
