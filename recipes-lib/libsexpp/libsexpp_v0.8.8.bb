SUMMARY = "RNP: high performance C++ OpenPGP library"
LICENSE += "BSD-2"
LIC_FILES_CHKSUM += "file://LICENSE.md;md5=b727b0ffd1939bd25361ab63fa947f8b"

inherit cmake

SRC_URI = "git://github.com/rnpgp/sexpp.git;protocol=https;nobranch=1"
SRCREV = "7197ff26eae37cc89c0b1eb88c365ca468ecd151"
S = "${WORKDIR}/git"

EXTRA_OECMAKE += " -DBUILD_SHARED_LIBS=ON -DWITH_SEXP_TESTS=OFF -DWITH_SEXP_CLI=OFF "

FILES:${PN} += "${libdir}/libsexpp${SOLIBS}"
