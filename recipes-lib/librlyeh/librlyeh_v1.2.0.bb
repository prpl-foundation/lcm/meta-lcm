SUMMARY = "The module librlyeh provides functions and datastructures to interact with OCI registries"
LICENSE += "BSD-2-Clause-Patent & SAH"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit pkgconfig config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/libraries/librlyeh.git;protocol=https;nobranch=1"
SRCREV = "945be3f276f9b83cc81d4e4f2da35a933d59d16b"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxj yajl libocispec openssl curl libsahtrace gpgme \
            libamxm liblcm libamxd"

RDEPENDS:${PN} += "libamxc libamxj yajl libocispec openssl curl libsahtrace \
                   gpgme libamxm liblcm libamxd"

LDFLAGS_append = " -lcurl "

FILES:${PN} += "/etc/amx/librlyeh/registry-auth.json"
FILES:${PN} += "/etc/amx/librlyeh/policy.json"
FILES:${PN} += "/etc/amx/librlyeh/sigstore.json"
FILES:${PN} += "${libdir}/librlyeh${SOLIBS}"
FILES:${PN}-dev += "${libdir}/librlyeh${SOLIBSDEV}"
FILES:${PN}-dev += "${includedir}/rlyeh/*.h"
