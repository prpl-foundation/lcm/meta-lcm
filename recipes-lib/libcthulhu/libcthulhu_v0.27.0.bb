SUMMARY = "The module libcthulhu provides functions and datastructures that are shared between Cthulhu and its backends"
LICENSE += "BSD-2-Clause-Patent & SAH & BSD-2-Clause-Patent"
LIC_FILES_CHKSUM += "file://LICENSE.BSD;md5=cd9db409406fd4c7234d852479547016"

inherit config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/libraries/libcthulhu.git;protocol=https;nobranch=1"
SRCREV = "5394dd64ad821cabca79a7646b3efd2bf7b2b4f6"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxj libocispec libsahtrace"
RDEPENDS:${PN} += "libamxc libamxj libocispec libsahtrace"

FILES:${PN} += "${libdir}/libcthulhu${SOLIBS}"
FILES:${PN}-dev += "${includedir}/cthulhu/*.h"
FILES:${PN}-dev += "${libdir}/libcthulhu${SOLIBSDEV}"
FILES:${PN}-staticdev += "${libdir}/libcthulhu.a"
