SUMMARY = "Botan - a cryptography toolkit for C++11"
LICENSE += "BSD-2"
LIC_FILES_CHKSUM += "file://license.txt;md5=f4ce98476c07c34e1793daa036960fad"

SRC_URI = "git://github.com/randombit/botan.git;protocol=https;nobranch=1"
SRCREV = "bfa8c29036ffc20736fb4b6eb7f6df4d7e44d3bb"
S = "${WORKDIR}/git"

do_configure() {
    python3 ./configure.py \
        --cpu=${TARGET_ARCH} \
        --with-sysroot-dir=${STAGING_DIR_TARGET} \
	--cc-bin="${STAGING_BINDIR_TOOLCHAIN}/${HOST_PREFIX}g++" \
        --cxxflags="${CXXFLAGS} ${HOST_CC_ARCH}${TOOLCHAIN_OPTIONS}" \
        --cc=gcc \
	--optimize-for-size
}

do_install() {
    install -d ${D}${libdir}
    install -d ${D}${includedir}/botan
    install -m 0755 ${S}/libbotan-* ${D}${libdir}
    install -m 0755 ${S}/build/include/botan/*.h ${D}${includedir}/botan/
}

SOLIBS = ".so"
SOLIBS += ".so.*"
FILES_SOLIBSDEV = ""
INSANE_SKIP:${PN} += "dev-so"

FILES:${PN} += "${libdir}/libbotan-2*"
FILES:${PN}-dev += "${includedir}/botan/*.h"
#FILES:${PN}-dev += "${libdir}/libbotan-2${SOLIBSDEV}"
