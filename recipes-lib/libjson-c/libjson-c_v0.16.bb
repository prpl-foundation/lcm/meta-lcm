SUMMARY = "JSON-C is a library aimed at easily constructing and parsing JSON objects in C"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://COPYING;md5=de54b60fbbc35123ba193fea8ee216f2"

inherit cmake

SRC_URI = "git://github.com/json-c/json-c.git;protocol=https;nobranch=1"
SRCREV = "2f2ddc1f2dbca56c874e8f9c31b5b963202d80e7"
S = "${WORKDIR}/git"

EXTRA_OECMAKE += " -DDISABLE_EXTRA_LIBS=TRUE "

FILES:${PN} += "${libdir}/libjson-c${SOLIBS}"
