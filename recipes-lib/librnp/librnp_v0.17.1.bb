SUMMARY = "RNP: high performance C++ OpenPGP library"
LICENSE += "BSD-2"
LIC_FILES_CHKSUM += "file://LICENSE.md;md5=d0efcdabf952630ab29ace9485a2e61d"

inherit cmake pkgconfig

SRC_URI = "git://github.com/rnpgp/rnp.git;protocol=https;nobranch=1"
SRCREV = "2e249423d617cf91714624a76bfe4ff613b41ac4"
S = "${WORKDIR}/git"

DEPENDS += "libsexpp libjson-c libbotan zlib bzip2"
RDEPENDS:${PN} += "libsexpp libbotan zlib bzip2"

EXTRA_OECMAKE += " -DBUILD_SHARED_LIBS=ON -DWITH_SEXP_TESTS=OFF -DSYSTEM_LIBSEXPP=ON -DBUILD_TESTING=OFF " 

FILES:${PN} += "${libdir}/librnp${SOLIBS}"
