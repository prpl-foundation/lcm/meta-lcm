SUMMARY = "The module liblcm provides common functions and datastructures for LCM components"
LICENSE += "BSD-2-Clause-Patent & SAH"
LIC_FILES_CHKSUM += "file://LICENSE;md5=cd9db409406fd4c7234d852479547016"

inherit config-prpl-lcm

SRC_URI = "git://gitlab.com/prpl-foundation/lcm/libraries/liblcm.git;protocol=https;nobranch=1"
SRCREV = "b985e3b5b4073c92f77d036be891f911aec1cd55"
S = "${WORKDIR}/git"

DEPENDS += "libamxc libamxo libamxp libamxd libamxp libsahtrace"
RDEPENDS:${PN} += "libamxc libamxo libamxp libamxd libamxp libsahtrace"

FILES:${PN} += "${libdir}/liblcm${SOLIBS}"
FILES:${PN}-dev += "${libdir}/liblcm${SOLIBSDEV}"
FILES:${PN}-dev += "${includedir}/lcm/*.h"
FILES:${PN}-staticdev += "${libdir}/liblcm.a"
