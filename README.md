
# meta-lcm
External yocto layer for Prpl LCM components.  
Runs LCM components: cthulhu, timingila and one of rlyeh or celephais

## Table of Contents

[[_TOC_]]

# Prpl LCM versions supported - 3.6.0

### Dependencies
The components in this layer depend on various Ambiorix libraries and components. These recipes are available in the meta-amx layer:
* [meta-amx](https://gitlab.com/prpl-foundation/components/ambiorix/meta-amx)

### Usage

Add this layer to an existing RDK-B yocto environment.

Configure the layer into the build by adding the following into `conf/bblayers.conf`
```
BBLAYERS += "${RDKROOT}/meta-lcm"
```  
List all recipes to verify that the layer is added correctly.
```
$ bitbake -s
```

## 1. Using Prpl LCM

### Life Cycle Management (LCM)


LCM refers to the management of remote residential gateway services offered by the home gateways and allow the ability to :

* Manage dynamically the services running on the system (add/configure/remove)
* Isolate and constraint the services managed by LCM from the rest of the system
* Use standard APIs for service integration to ensure services' portability across different software stacks.



LCM aims to enhance modularity, security, and the management of service dependencies in relation to the core system. It achieves this by providing portable and isolated services that communicate with each other and the system using standardized high-level APIs. This approach enables seamless integration regardless of the underlying implementation at the lower levels.

An LCM agent is responsible for ensuring Life Cycle Management within the home device system and handles standard service management functionalities like installation, updating, and uninstallation.

An LCM backend is responsible for driving the LCM agent remotely through standard APIs.

In prpl ecosystem offers an open-source implementation of the LCM agent. The LCM agent achieves its objectives of managing modular service by utilizing OCI images or OCI bundles as packages and Linux containers (lxc) or crun as containers for services. Comprising various applications, modules, and plugins, the LCM agent incorporates core functionalities named Timingila, Rlyeh, Celephais and Cthulhu.

### Overview of the Prpl LCM agent
The implementation of the LCM agent can by found on gitlab-lcm (https://gitlab.com/prpl-foundation/lcm).  
The different components are divided over a set of sub-directories.
* applications: standalone main LCM component implemented as Ambiorix plugins.
* libraries: the core libraries of the LCM agent.
* modules: dynamically loadable modules implementing applications' backend (lower layer)

[Prpl LCM HLD](https://gitlab.com/prpl-foundation/lcm/development/vagrant/-/blob/master/images/Prpl_LCM_HLD.png)

#### Timingila
Within the LCM agent, Timingila assumes a main role as a component responsible for implementing the standard LCM APIs defined by the Broadband Forum TR-181, specifically the SoftwareModules section. By adhering to these standard APIs, Timingila ensures compatibility and interoperability within the LCM ecosystem (i.e the backend controlling the LCM agent)

To achieve this, Timingila leverages dedicated dynamic modules that establish the interface between the LCM agent's higher layers (Rlyeh or Celephais) and its lower layers (Cthulhu). These dynamic modules are designed to handle the implementation of underlying technologies, serving as the bridge between the LCM APIs and the specific lower-level components.

By employing dedicated dynamic modules, Timingila facilitates seamless communication and interaction with the lower layers, enabling the LCM agent to effectively manage various technologies and integrate them within the broader system. This modular approach enhances flexibility, scalability, and adaptability, allowing the LCM agent to accommodate diverse underlying technologies while maintaining a standardized interface defined by the TR-181 LCM APIs.



#### Rlyeh
Rlyeh plays a crucial role as an OCI image manager, overseeing a range of operations related to OCI images. Its responsibilities encompass tasks such as downloading images, verifying their integrity, storing them locally, managing versions of images, and facilitating their removal when necessary. To accomplish these functions, Rlyeh leverages the capabilities provided by two key libraries: libcurl and libocispec.

By utilizing libcurl, Rlyeh gains the ability to establish network connections, perform HTTP requests, and retrieve OCI images from remote OCI registries. This allows for efficient downloading and retrieval of images while ensuring data integrity during the transfer process.

Additionally, Rlyeh relies on libocispec to implement its core functionalities. libocispec provides the necessary tools and mechanisms to handle various aspects of OCI images, including parsing image specifications, validating image configurations, and performing essential operations such as version management and image removal.

Through the combined utilization of libcurl and libocispec, Rlyeh ensures robust management and manipulation of OCI images, enabling seamless integration into the broader system architecture.



#### Celephais
Celephais packager application  handles the download and management of OCI bundles, along with associated adaptor and support libraries, timingila-celephais and libcelephais respectively. It is an alternative to Rlyeh for working with OCI bundles instead of OCI images.



#### Cthulhu
Cthulhu assumes the role of a container manager within the LCM agent system, tasked with converting OCI images obtained through Rlyeh into Linux containers. This conversion process involves transforming the pulled OCI images into a format compatible with container runtime such as lxc or crun. Once the containers are created, Cthulhu utilizes the designated container runtime (through loadable modules) to effectively execute and manage the running containers. By leveraging container runtime, Cthulhu ensures the proper execution and control of the Linux containers, enabling seamless integration of containerized applications within the system environment.

Cthulhu manages also "Sandboxes".

A Sandbox defines restrictions on a container or group of containers, based on

* cgroups
    * CPU usage
    * memory usage
    * access to devices

* Namespaces
    * Network namespace - hide host network from container
    * PID namespace - hide host processes from container
    * User namespace


A Sandbox may also provide persistent storage in the form of a filesystem image, isolating the processes within the Sandbox from the host filesystem.

Sandboxes can be nested: typically the outermost layer(s) will correspond to Execution Environment(s) in the TR-181 Data Model, while the innermost Sandboxes will each correspond to a single container (Execution Unit).



#### Configuration
The Prpl LCM system is highly configurable and a number of modules exist for different container technologies i.e. cthulhu-lxc and cthulhu-crun and for bus systems i.e. ubus and rbus.

By default prplLCM on RDK-B is built with support for OCI images and lxc runtime. The decision is based on enabled `DISTRO_FEATURES`. In order to switch to OCI bundles and crun runtime change:

```bash
DISTRO_FEATURES_append = " lcm-images lxc-backend "
```
into
```bash
DISTRO_FEATURES_append = " lcm-bundles crun-backend "
```



#### Examples
This section assumes that you are using cthulhu, rlyeh, timingila, cthulhu-lxc, and amxb-rbus and amxb-ubus.

This section also assumes that you do not care about which container is used, it uses an Alpine container from docker as an example, any OCI compliant container should work, but some may require different settings or have different behaviors (e.g. may run and then exit) which this section will not cover.

All datamodels supported are documented here - https://prpl-foundation.gitlab.io/prplos/feeds/feed-prpl/SoftwareModules..html
Note: Since this is a development environment, we sometimes use Vagrant versions that include features not yet documented.

### Container lifecycle with rbus and amxb-rbus

To make the prpl LCM portable to RDK-B and rbus, prpl foundation developed the rbus backend to be able to abstract the bus system.
The amxb-rbus is currently in a stable version and all features are working. The project is available here - https://gitlab.com/prpl-foundation/components/ambiorix/modules/amxb_backends/amxb_rbus

#### Bus Agnostic Interactive Command Line Interface - ba-cli
Among the various OS-independent projects, prpl has developed the ba-cli utility. This Bus Agnostic Interactive Command Line Interface can be utilized for testing. It boasts several appealing features that enhance rbuscli, including an autocomplete discovery mechanism and improved human-readable output.

The project can be found here - https://gitlab.com/prpl-foundation/components/ambiorix/modules/amx_cli/mod-ba-cli

In the following examples, the ba-cli tool will be utilized. However, rbuscli and ubus are also viable alternatives.

#### Setup

A startup script is included as part of the Vagrantfile. This script initiates rbus and the Prpl components as background processes.

To start rbus and Prpl components, run:

```bash
~/prpl-lcm-startup-rbus.sh
```

Alternatively, to start Prpl LCM using ubus, run:
```
~/prpl-lcm-startup-ubus.sh
```

During testing, you may need to stop and clean the environment. A script is available to forcefully kill processes and remove related files:
```
~/prpl-lcm-force_cleanup.sh
```

After the script has finished running, the Prpl LCM stack should be up and running.

You can subscribe to the rbus events via

#### Subscriptios and event
##### rbuscli
```bash
rbuscli -i
rbuscli> subscribe Device.SoftwareModules.DUStateChange!
```

##### ba-cli
```bash
ba-cli:
Device.SoftwareModules.?&
```

Before starting the installation, is possible to verify if the environment is working and if the ExecutionEnvironment is available and running:

```bash
root - * - [bus-cli] (0)
 > Device.SoftwareModules.?
Device.SoftwareModules.
Device.SoftwareModules.DeploymentUnitNumberOfEntries=0
Device.SoftwareModules.ExecEnvNumberOfEntries=1
Device.SoftwareModules.ExecutionUnitNumberOfEntries=0
Device.SoftwareModules.ExecEnv.1.
Device.SoftwareModules.ExecEnv.1.ActiveExecutionUnits=""
Device.SoftwareModules.ExecEnv.1.Alias="cpe-generic"
Device.SoftwareModules.ExecEnv.1.AllocatedCPUPercent=100
Device.SoftwareModules.ExecEnv.1.AllocatedDiskSpace=5120
Device.SoftwareModules.ExecEnv.1.AllocatedMemory=-1
Device.SoftwareModules.ExecEnv.1.AvailableDiskSpace=4920
Device.SoftwareModules.ExecEnv.1.AvailableMemory=-1
Device.SoftwareModules.ExecEnv.1.CreatedAt=2024-07-04T23:50:43Z
Device.SoftwareModules.ExecEnv.1.CurrentRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Description=""
Device.SoftwareModules.ExecEnv.1.Enable=true
Device.SoftwareModules.ExecEnv.1.InitialExecutionUnitRunLevel=-1
Device.SoftwareModules.ExecEnv.1.InitialRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Name="generic"
Device.SoftwareModules.ExecEnv.1.ParentExecEnv=""
Device.SoftwareModules.ExecEnv.1.ProcessorRefList=""
Device.SoftwareModules.ExecEnv.1.RestartReason=""
Device.SoftwareModules.ExecEnv.1.Status="Up"
Device.SoftwareModules.ExecEnv.1.Type="lxc:5.0.2"
Device.SoftwareModules.ExecEnv.1.Vendor="Cthulhu"
Device.SoftwareModules.ExecEnv.1.Version="3.8.0"
Device.SoftwareModules.NetworkConfig.
Device.SoftwareModules.NetworkConfig.DefaultBridge="br-lcm"
Device.SoftwareModules.NetworkConfig.DefaultFirewallChain="Device.Firewall.Chain.[Alias=="LCM"]."
Device.SoftwareModules.NetworkConfig.Interfaces.1.
Device.SoftwareModules.NetworkConfig.Interfaces.1.Name="Wan"
Device.SoftwareModules.NetworkConfig.Interfaces.1.Reference="Device.Logical.Interface.1."
Device.SoftwareModules.NetworkConfig.Interfaces.2.
Device.SoftwareModules.NetworkConfig.Interfaces.2.Name="Lan"
Device.SoftwareModules.NetworkConfig.Interfaces.2.Reference="Device.Logical.Interface.2."
```

```bash
root@lcm-vagrant-focal-rbus:/home/vagrant# rbuscli getv Device.SoftwareModules.
Parameter  1:
              Name  : Device.SoftwareModules.ExecutionUnitNumberOfEntries
              Type  : uint32
              Value : 0
Parameter  2:
              Name  : Device.SoftwareModules.ExecEnvNumberOfEntries
              Type  : uint32
              Value : 1
Parameter  3:
              Name  : Device.SoftwareModules.DeploymentUnitNumberOfEntries
              Type  : uint32
              Value : 0
Parameter  4:
              Name  : Device.SoftwareModules.ExecEnv.1.AvailableMemory
              Type  : int64
              Value : -1
Parameter  5:
              Name  : Device.SoftwareModules.ExecEnv.1.Status
              Type  : string
              Value : Up
Parameter  6:
              Name  : Device.SoftwareModules.ExecEnv.1.AllocatedDiskSpace
              Type  : int32
              Value : 5120
Parameter  7:
              Name  : Device.SoftwareModules.ExecEnv.1.RestartReason
              Type  : string
              Value : 
Parameter  8:
              Name  : Device.SoftwareModules.ExecEnv.1.Enable
              Type  : boolean
              Value : 1
Parameter  9:
              Name  : Device.SoftwareModules.ExecEnv.1.Name
              Type  : string
              Value : generic
Parameter 10:
              Name  : Device.SoftwareModules.ExecEnv.1.ActiveExecutionUnits
              Type  : string
              Value : 
Parameter 11:
              Name  : Device.SoftwareModules.ExecEnv.1.InitialRunLevel
              Type  : int16
              Value : -1
Parameter 12:
              Name  : Device.SoftwareModules.ExecEnv.1.Type
              Type  : string
              Value : lxc:5.0.2
Parameter 13:
              Name  : Device.SoftwareModules.ExecEnv.1.InitialExecutionUnitRunLevel
              Type  : int16
              Value : -1
Parameter 14:
              Name  : Device.SoftwareModules.ExecEnv.1.CurrentRunLevel
              Type  : int16
              Value : -1
Parameter 15:
              Name  : Device.SoftwareModules.ExecEnv.1.AvailableDiskSpace
              Type  : int64
              Value : 4920
Parameter 16:
              Name  : Device.SoftwareModules.ExecEnv.1.ProcessorRefList
              Type  : string
              Value : 
Parameter 17:
              Name  : Device.SoftwareModules.ExecEnv.1.Description
              Type  : string
              Value : 
Parameter 18:
              Name  : Device.SoftwareModules.ExecEnv.1.Vendor
              Type  : string
              Value : Cthulhu
Parameter 19:
              Name  : Device.SoftwareModules.ExecEnv.1.AllocatedMemory
              Type  : int32
              Value : -1
Parameter 20:
              Name  : Device.SoftwareModules.ExecEnv.1.ParentExecEnv
              Type  : string
              Value : 
Parameter 21:
              Name  : Device.SoftwareModules.ExecEnv.1.CreatedAt
              Type  : datetime
              Value : 2024-07-04T23:50:43Z
Parameter 22:
              Name  : Device.SoftwareModules.ExecEnv.1.AllocatedCPUPercent
              Type  : int32
              Value : 100
Parameter 23:
              Name  : Device.SoftwareModules.ExecEnv.1.Alias
              Type  : string
              Value : cpe-generic
Parameter 24:
              Name  : Device.SoftwareModules.ExecEnv.1.Version
              Type  : string
              Value : 3.8.0
Parameter 25:
              Name  : Device.SoftwareModules.NetworkConfig.DefaultFirewallChain
              Type  : string
              Value : Device.Firewall.Chain.[Alias=="LCM"].
Parameter 26:
              Name  : Device.SoftwareModules.NetworkConfig.DefaultBridge
              Type  : string
              Value : br-lcm
Parameter 27:
              Name  : Device.SoftwareModules.NetworkConfig.Interfaces.2.Reference
              Type  : string
              Value : Device.Logical.Interface.2.
Parameter 28:
              Name  : Device.SoftwareModules.NetworkConfig.Interfaces.2.Name
              Type  : string
              Value : Lan
Parameter 29:
              Name  : Device.SoftwareModules.NetworkConfig.Interfaces.1.Reference
              Type  : string
              Value : Device.Logical.Interface.1.
Parameter 30:
              Name  : Device.SoftwareModules.NetworkConfig.Interfaces.1.Name
              Type  : string
              Value : Wan
```

Prpl LCM by default has one ExecEnv enabled and should have the Device.SoftwareModules.ExecEnv.1.Status="Up"


In the following example, an open-srouce container will e installed from the docker public OCI registry. The alpine container will be used in the next steps.

Using lxc-ls command is possible to see the containers installed. At this phase we don't have any installed container:

```bash
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-ls -f
root@lcm-vagrant-focal-rbus:/home/vagrant#
```

#### Install Container

This is a vagrant environment and an amd64 architecture container will be used. The architecture of the container needs to be adapted to the CPE architecthure.

To generate a valid UUID, the tool "**UUID Version-5 Generator**" can be used - https://www.uuidtools.com/v5

##### ba-cli
```bash
Device.SoftwareModules.InstallDU(URL = "https://index.docker.io/amd64/alpine:3.18", UUID = "10000000-0971-52b1-b70c-47ab6ce48128", ExecutionEnvRef = "Device.SoftwareModules.ExecEnv.1.", AutoStart = "true")
```
This command may take a few seconds to run depending on your internet connection as it needs to go and grab the container from the URI provided.

When using ba-cli, is important to add a subscription for the Device.SoftwareModules. in order to see the updates and the DUStateChange! event.
Below the output:

Ouput of Alpine Installation
```bash
root - * - [bus-cli] (0)
 > Device.SoftwareModules.?&
Added subscription for Device.SoftwareModules.
root - * - [bus-cli] (0)
 > Device.SoftwareModules.InstallDU(URL = "https://index.docker.io/amd64/alpine:3.18", UUID = "10000000-0971-52b1-b70c-47ab6ce48128", ExecutionEnvRef = "generic", AutoStart = "true")

[2024-07-05T00:05:56Z] Event dm:object-changed received from Device.SoftwareModules.
<  dm:object-changed> Device.SoftwareModules.DeploymentUnitNumberOfEntries = 0 -> 1

[2024-07-05T00:05:56Z] Event dm:instance-added received from Device.SoftwareModules.DeploymentUnit.
<  dm:instance-added> Device.SoftwareModules.DeploymentUnit.1.VendorConfigList = 
<                   > Device.SoftwareModules.DeploymentUnit.1.Status = Installing
<                   > Device.SoftwareModules.DeploymentUnit.1.VendorLogList = 
<                   > Device.SoftwareModules.DeploymentUnit.1.UUID = 10000000-0971-52b1-b70c-47ab6ce48128
<                   > Device.SoftwareModules.DeploymentUnit.1.ExecutionEnvRef = Device.SoftwareModules.ExecEnv.1.
<                   > Device.SoftwareModules.DeploymentUnit.1.Name = amd64/alpine
<                   > Device.SoftwareModules.DeploymentUnit.1.ModuleVersion = 
<                   > Device.SoftwareModules.DeploymentUnit.1.ExecutionUnitList = 
<                   > Device.SoftwareModules.DeploymentUnit.1.URL = https://index.docker.io/amd64/alpine:3.18
<                   > Device.SoftwareModules.DeploymentUnit.1.Resolved = true
<                   > Device.SoftwareModules.DeploymentUnit.1.DUID = edefaf21-2e07-5a48-bae5-85a516686e10
<                   > Device.SoftwareModules.DeploymentUnit.1.Description = Unknown
<                   > Device.SoftwareModules.DeploymentUnit.1.Vendor = Unknown
<                   > Device.SoftwareModules.DeploymentUnit.1.LastUpdate = 0001-01-01T00:00:00Z
<                   > Device.SoftwareModules.DeploymentUnit.1.Alias = cpe-edefaf21-2e07-5a48-bae5-85a516686e10
<                   > Device.SoftwareModules.DeploymentUnit.1.Version = 3.18
<                   > Device.SoftwareModules.DeploymentUnit.1.Installed = 0001-01-01T00:00:00Z
Device.SoftwareModules.InstallDU() returned
[
    <NULL>
]

[2024-07-05T00:05:56Z] Event dm:object-changed received from Device.SoftwareModules.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnitNumberOfEntries = 0 -> 1

[2024-07-05T00:05:56Z] Event dm:instance-added received from Device.SoftwareModules.ExecutionUnit.
<  dm:instance-added> Device.SoftwareModules.ExecutionUnit.1.VendorConfigList = 
<                   > Device.SoftwareModules.ExecutionUnit.1.MemoryInUse = 948
<                   > Device.SoftwareModules.ExecutionUnit.1.RunLevel = 0
<                   > Device.SoftwareModules.ExecutionUnit.1.AvailableMemory = -1
<                   > Device.SoftwareModules.ExecutionUnit.1.AutoStart = true
<                   > Device.SoftwareModules.ExecutionUnit.1.ExecEnvLabel = edefaf21-2e07-5a48-bae5-85a516686e10
<                   > Device.SoftwareModules.ExecutionUnit.1.ExecutionFaultMessage = 
<                   > Device.SoftwareModules.ExecutionUnit.1.VendorLogList = 
<                   > Device.SoftwareModules.ExecutionUnit.1.Status = Idle
<                   > Device.SoftwareModules.ExecutionUnit.1.AllocatedDiskSpace = -1
<                   > Device.SoftwareModules.ExecutionUnit.1.ExecutionEnvRef = generic
<                   > Device.SoftwareModules.ExecutionUnit.1.Name = amd64/alpine
<                   > Device.SoftwareModules.ExecutionUnit.1.AssociatedProcessList = 
<                   > Device.SoftwareModules.ExecutionUnit.1.ExecutionFaultCode = NoFault
<                   > Device.SoftwareModules.ExecutionUnit.1.AvailableDiskSpace = 4896
<                   > Device.SoftwareModules.ExecutionUnit.1.Vendor = 
<                   > Device.SoftwareModules.ExecutionUnit.1.Description = 
<                   > Device.SoftwareModules.ExecutionUnit.1.AllocatedMemory = -1
<                   > Device.SoftwareModules.ExecutionUnit.1.DiskSpaceInUse = 48
<                   > Device.SoftwareModules.ExecutionUnit.1.AllocatedCPUPercent = -1
<                   > Device.SoftwareModules.ExecutionUnit.1.EUID = edefaf21-2e07-5a48-bae5-85a516686e10
<                   > Device.SoftwareModules.ExecutionUnit.1.References = 
<                   > Device.SoftwareModules.ExecutionUnit.1.Alias = cpe-edefaf21-2e07-5a48-bae5-85a516686e10
<                   > Device.SoftwareModules.ExecutionUnit.1.Version = 3.18

[2024-07-05T00:05:56Z] Event dm:object-added received from Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.
<    dm:object-added> Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.

[2024-07-05T00:05:56Z] Event dm:object-added received from Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.AccessInterfaces.
<    dm:object-added> Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.AccessInterfaces.

[2024-07-05T00:05:56Z] Event dm:object-added received from Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.PortForwarding.
<    dm:object-added> Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.PortForwarding.

[2024-07-05T00:05:56Z] Event dm:object-added received from Device.SoftwareModules.ExecutionUnit.1.HostObject.
<    dm:object-added> Device.SoftwareModules.ExecutionUnit.1.HostObject.

[2024-07-05T00:05:56Z] Event dm:object-added received from Device.SoftwareModules.ExecutionUnit.1.EnvVariable.
<    dm:object-added> Device.SoftwareModules.ExecutionUnit.1.EnvVariable.

[2024-07-05T00:05:56Z] Event dm:object-added received from Device.SoftwareModules.ExecutionUnit.1.Extensions.
<    dm:object-added> Device.SoftwareModules.ExecutionUnit.1.Extensions.

[2024-07-05T00:05:56Z] Event dm:object-changed received from Device.SoftwareModules.DeploymentUnit.1.
<  dm:object-changed> Device.SoftwareModules.DeploymentUnit.1.LastUpdate = 0001-01-01T00:00:00Z -> 1970-01-01T00:00:00Z
<                   > Device.SoftwareModules.DeploymentUnit.1.Status = Installing -> Installed
<                   > Device.SoftwareModules.DeploymentUnit.1.Installed = 0001-01-01T00:00:00Z -> 2024-07-05T00:05:56Z

[2024-07-05T00:05:56Z] Event DUStateChange! received from Device.SoftwareModules.DeploymentUnit.1.
{
    data = {
        CurrentState = "",
        ExecutionEnvRef = "Device.SoftwareModules.ExecEnv.1.",
        ExecutionUnitRefList = "Device.SoftwareModules.ExecutionUnit.1",
        Fault.FaultCode = 0,
        Fault.FaultString = "",
        OperationPerformed = "Install",
        Resolved = true,
        UUID = "10000000-0971-52b1-b70c-47ab6ce48128",
        Version = "3.18"
    },
    eobject = "Device.SoftwareModules.DeploymentUnit.[cpe-edefaf21-2e07-5a48-bae5-85a516686e10].",
    notification = "DUStateChange!",
    object = "Device.SoftwareModules.DeploymentUnit.cpe-edefaf21-2e07-5a48-bae5-85a516686e10.",
    path = "Device.SoftwareModules.DeploymentUnit.1."
}

[2024-07-05T00:05:56Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.1.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.1.Status = Idle -> Starting

[2024-07-05T00:05:56Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.1.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.1.Status = Starting -> Active

root - * - [bus-cli] (0)
 > 
```

Confirmation that the Alpine container was succesfully Installed:
```bash
root - * - [bus-cli] (0)
 > Device.SoftwareModules.? 
Device.SoftwareModules.
Device.SoftwareModules.DeploymentUnitNumberOfEntries=1
Device.SoftwareModules.ExecEnvNumberOfEntries=1
Device.SoftwareModules.ExecutionUnitNumberOfEntries=1
Device.SoftwareModules.DeploymentUnit.1.
Device.SoftwareModules.DeploymentUnit.1.Alias="cpe-edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.DeploymentUnit.1.DUID="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.DeploymentUnit.1.Description="Unknown"
Device.SoftwareModules.DeploymentUnit.1.ExecutionEnvRef="Device.SoftwareModules.ExecEnv.1."
Device.SoftwareModules.DeploymentUnit.1.ExecutionUnitList=""
Device.SoftwareModules.DeploymentUnit.1.Installed=2024-07-05T00:05:56Z
Device.SoftwareModules.DeploymentUnit.1.LastUpdate=1970-01-01T00:00:00Z
Device.SoftwareModules.DeploymentUnit.1.ModuleVersion=""
Device.SoftwareModules.DeploymentUnit.1.Name="amd64/alpine"
Device.SoftwareModules.DeploymentUnit.1.Resolved=true
Device.SoftwareModules.DeploymentUnit.1.Status="Installed"
Device.SoftwareModules.DeploymentUnit.1.URL="https://index.docker.io/amd64/alpine:3.18"
Device.SoftwareModules.DeploymentUnit.1.UUID="10000000-0971-52b1-b70c-47ab6ce48128"
Device.SoftwareModules.DeploymentUnit.1.Vendor="Unknown"
Device.SoftwareModules.DeploymentUnit.1.VendorConfigList=""
Device.SoftwareModules.DeploymentUnit.1.VendorLogList=""
Device.SoftwareModules.DeploymentUnit.1.Version="3.18"
Device.SoftwareModules.ExecEnv.1.
Device.SoftwareModules.ExecEnv.1.ActiveExecutionUnits=""
Device.SoftwareModules.ExecEnv.1.Alias="cpe-generic"
Device.SoftwareModules.ExecEnv.1.AllocatedCPUPercent=100
Device.SoftwareModules.ExecEnv.1.AllocatedDiskSpace=5120
Device.SoftwareModules.ExecEnv.1.AllocatedMemory=-1
Device.SoftwareModules.ExecEnv.1.AvailableDiskSpace=4896
Device.SoftwareModules.ExecEnv.1.AvailableMemory=-1
Device.SoftwareModules.ExecEnv.1.CreatedAt=2024-07-04T23:50:43Z
Device.SoftwareModules.ExecEnv.1.CurrentRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Description=""
Device.SoftwareModules.ExecEnv.1.Enable=true
Device.SoftwareModules.ExecEnv.1.InitialExecutionUnitRunLevel=-1
Device.SoftwareModules.ExecEnv.1.InitialRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Name="generic"
Device.SoftwareModules.ExecEnv.1.ParentExecEnv=""
Device.SoftwareModules.ExecEnv.1.ProcessorRefList=""
Device.SoftwareModules.ExecEnv.1.RestartReason=""
Device.SoftwareModules.ExecEnv.1.Status="Up"
Device.SoftwareModules.ExecEnv.1.Type="lxc:5.0.2"
Device.SoftwareModules.ExecEnv.1.Vendor="Cthulhu"
Device.SoftwareModules.ExecEnv.1.Version="3.8.0"
Device.SoftwareModules.ExecutionUnit.1.
Device.SoftwareModules.ExecutionUnit.1.Alias="cpe-edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.1.AllocatedCPUPercent=-1
Device.SoftwareModules.ExecutionUnit.1.AllocatedDiskSpace=-1
Device.SoftwareModules.ExecutionUnit.1.AllocatedMemory=-1
Device.SoftwareModules.ExecutionUnit.1.AssociatedProcessList=""
Device.SoftwareModules.ExecutionUnit.1.AutoStart=true
Device.SoftwareModules.ExecutionUnit.1.AvailableDiskSpace=4896
Device.SoftwareModules.ExecutionUnit.1.AvailableMemory=-1
Device.SoftwareModules.ExecutionUnit.1.Description=""
Device.SoftwareModules.ExecutionUnit.1.DiskSpaceInUse=48
Device.SoftwareModules.ExecutionUnit.1.EUID="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.1.ExecEnvLabel="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.1.ExecutionEnvRef="generic"
Device.SoftwareModules.ExecutionUnit.1.ExecutionFaultCode="NoFault"
Device.SoftwareModules.ExecutionUnit.1.ExecutionFaultMessage=""
Device.SoftwareModules.ExecutionUnit.1.MemoryInUse=948
Device.SoftwareModules.ExecutionUnit.1.Name="amd64/alpine"
Device.SoftwareModules.ExecutionUnit.1.References=""
Device.SoftwareModules.ExecutionUnit.1.RunLevel=0
Device.SoftwareModules.ExecutionUnit.1.Status="Active"
Device.SoftwareModules.ExecutionUnit.1.Vendor=""
Device.SoftwareModules.ExecutionUnit.1.VendorConfigList=""
Device.SoftwareModules.ExecutionUnit.1.VendorLogList=""
Device.SoftwareModules.ExecutionUnit.1.Version="3.18"
Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.
Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.X_PRPL-COM_DNSSDRefList=""
Device.SoftwareModules.NetworkConfig.
Device.SoftwareModules.NetworkConfig.DefaultBridge="br-lcm"
Device.SoftwareModules.NetworkConfig.DefaultFirewallChain="Device.Firewall.Chain.[Alias=="LCM"]."
Device.SoftwareModules.NetworkConfig.Interfaces.1.
Device.SoftwareModules.NetworkConfig.Interfaces.1.Name="Wan"
Device.SoftwareModules.NetworkConfig.Interfaces.1.Reference="Device.Logical.Interface.1."
Device.SoftwareModules.NetworkConfig.Interfaces.2.
Device.SoftwareModules.NetworkConfig.Interfaces.2.Name="Lan"
Device.SoftwareModules.NetworkConfig.Interfaces.2.Reference="Device.Logical.Interface.2."

root - * - [bus-cli] (0)
```
lxc-ls -f
```bash
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-ls -f
NAME                                 STATE   AUTOSTART GROUPS IPV4                     IPV6 UNPRIVILEGED 
edefaf21-2e07-5a48-bae5-85a516686e10 RUNNING 0         -      10.0.2.15, 192.168.200.8 -    false        
```

Attach to the container / Login into the container
```bash
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-ls -f
NAME                                 STATE   AUTOSTART GROUPS IPV4                     IPV6 UNPRIVILEGED 
edefaf21-2e07-5a48-bae5-85a516686e10 RUNNING 0         -      10.0.2.15, 192.168.200.8 -    false        
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-attach edefaf21-2e07-5a48-bae5-85a516686e10
/ # ls
bin    dev    etc    home   lib    media  mnt    opt    proc   root   run    sbin   srv    sys    tmp    usr    var
/ # uname -a
Linux edefaf21-2e07-5a48-bae5-85a516686e10 5.4.0-173-generic #191-Ubuntu SMP Fri Feb 2 13:55:07 UTC 2024 x86_64 Linux
/ # cat /etc/alpine-release 
3.18.7
/ # 
```

##### rbuscli
```bash
rbuscli method_values "Device.SoftwareModules.InstallDU()" URL string "https://index.docker.io/amd64/alpine:3.18" UUID string "10000000-0971-52b1-b70c-47ab6ce48128" ExecutionEnvRef string "Device.SoftwareModules.ExecEnv.1." AutoStart string "true"
```

#### Stop the Container

##### ba-cli
```bash
Device.SoftwareModules.ExecutionUnit.1.SetRequestedState(RequestedState = "Idle")
```

The container state can be checked with the same commands as described before and subscribing for the events.
This time is important to check for ExecutionUnit `Status` to be `Idle` and lxc-ls showing the alpine container in the STOPPED state.


```bash
root - * - [bus-cli] (0)
 > Device.SoftwareModules.?&
Added subscription for Device.SoftwareModules.

root - * - [bus-cli] (0)
 > Device.SoftwareModules.ExecutionUnit.1.SetRequestedState(RequestedState = "Idle")
Device.SoftwareModules.ExecutionUnit.1.SetRequestedState() returned
[
    1
]

[2024-07-05T00:25:24Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.1.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.1.Status = Active -> Stopping

root - * - [bus-cli] (0)
 > 

[2024-07-05T00:25:35Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.1.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.1.Status = Stopping -> Idle

root - * - [bus-cli] (0)
 > 

root - * - [bus-cli] (0)
 > Device.SoftwareModules.? 
Device.SoftwareModules.
Device.SoftwareModules.DeploymentUnitNumberOfEntries=1
Device.SoftwareModules.ExecEnvNumberOfEntries=1
Device.SoftwareModules.ExecutionUnitNumberOfEntries=1
Device.SoftwareModules.DeploymentUnit.1.
Device.SoftwareModules.DeploymentUnit.1.Alias="cpe-edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.DeploymentUnit.1.DUID="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.DeploymentUnit.1.Description="Unknown"
Device.SoftwareModules.DeploymentUnit.1.ExecutionEnvRef="Device.SoftwareModules.ExecEnv.1."
Device.SoftwareModules.DeploymentUnit.1.ExecutionUnitList=""
Device.SoftwareModules.DeploymentUnit.1.Installed=2024-07-05T00:05:56Z
Device.SoftwareModules.DeploymentUnit.1.LastUpdate=1970-01-01T00:00:00Z
Device.SoftwareModules.DeploymentUnit.1.ModuleVersion=""
Device.SoftwareModules.DeploymentUnit.1.Name="amd64/alpine"
Device.SoftwareModules.DeploymentUnit.1.Resolved=true
Device.SoftwareModules.DeploymentUnit.1.Status="Installed"
Device.SoftwareModules.DeploymentUnit.1.URL="https://index.docker.io/amd64/alpine:3.18"
Device.SoftwareModules.DeploymentUnit.1.UUID="10000000-0971-52b1-b70c-47ab6ce48128"
Device.SoftwareModules.DeploymentUnit.1.Vendor="Unknown"
Device.SoftwareModules.DeploymentUnit.1.VendorConfigList=""
Device.SoftwareModules.DeploymentUnit.1.VendorLogList=""
Device.SoftwareModules.DeploymentUnit.1.Version="3.18"
Device.SoftwareModules.ExecEnv.1.
Device.SoftwareModules.ExecEnv.1.ActiveExecutionUnits=""
Device.SoftwareModules.ExecEnv.1.Alias="cpe-generic"
Device.SoftwareModules.ExecEnv.1.AllocatedCPUPercent=100
Device.SoftwareModules.ExecEnv.1.AllocatedDiskSpace=5120
Device.SoftwareModules.ExecEnv.1.AllocatedMemory=-1
Device.SoftwareModules.ExecEnv.1.AvailableDiskSpace=4880
Device.SoftwareModules.ExecEnv.1.AvailableMemory=-1
Device.SoftwareModules.ExecEnv.1.CreatedAt=2024-07-04T23:50:43Z
Device.SoftwareModules.ExecEnv.1.CurrentRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Description=""
Device.SoftwareModules.ExecEnv.1.Enable=true
Device.SoftwareModules.ExecEnv.1.InitialExecutionUnitRunLevel=-1
Device.SoftwareModules.ExecEnv.1.InitialRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Name="generic"
Device.SoftwareModules.ExecEnv.1.ParentExecEnv=""
Device.SoftwareModules.ExecEnv.1.ProcessorRefList=""
Device.SoftwareModules.ExecEnv.1.RestartReason=""
Device.SoftwareModules.ExecEnv.1.Status="Up"
Device.SoftwareModules.ExecEnv.1.Type="lxc:5.0.2"
Device.SoftwareModules.ExecEnv.1.Vendor="Cthulhu"
Device.SoftwareModules.ExecEnv.1.Version="3.8.0"
Device.SoftwareModules.ExecutionUnit.1.
Device.SoftwareModules.ExecutionUnit.1.Alias="cpe-edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.1.AllocatedCPUPercent=-1
Device.SoftwareModules.ExecutionUnit.1.AllocatedDiskSpace=-1
Device.SoftwareModules.ExecutionUnit.1.AllocatedMemory=-1
Device.SoftwareModules.ExecutionUnit.1.AssociatedProcessList=""
Device.SoftwareModules.ExecutionUnit.1.AutoStart=true
Device.SoftwareModules.ExecutionUnit.1.AvailableDiskSpace=-1
Device.SoftwareModules.ExecutionUnit.1.AvailableMemory=-1
Device.SoftwareModules.ExecutionUnit.1.Description=""
Device.SoftwareModules.ExecutionUnit.1.DiskSpaceInUse=-1
Device.SoftwareModules.ExecutionUnit.1.EUID="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.1.ExecEnvLabel="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.1.ExecutionEnvRef="generic"
Device.SoftwareModules.ExecutionUnit.1.ExecutionFaultCode="NoFault"
Device.SoftwareModules.ExecutionUnit.1.ExecutionFaultMessage=""
Device.SoftwareModules.ExecutionUnit.1.MemoryInUse=-1
Device.SoftwareModules.ExecutionUnit.1.Name="amd64/alpine"
Device.SoftwareModules.ExecutionUnit.1.References=""
Device.SoftwareModules.ExecutionUnit.1.RunLevel=0
             c"Idle"
Device.SoftwareModules.ExecutionUnit.1.Vendor=""
Device.SoftwareModules.ExecutionUnit.1.VendorConfigList=""
Device.SoftwareModules.ExecutionUnit.1.VendorLogList=""
Device.SoftwareModules.ExecutionUnit.1.Version="3.18"
Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.
Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.X_PRPL-COM_DNSSDRefList=""
Device.SoftwareModules.NetworkConfig.
Device.SoftwareModules.NetworkConfig.DefaultBridge="br-lcm"
Device.SoftwareModules.NetworkConfig.DefaultFirewallChain="Device.Firewall.Chain.[Alias=="LCM"]."
Device.SoftwareModules.NetworkConfig.Interfaces.1.
Device.SoftwareModules.NetworkConfig.Interfaces.1.Name="Wan"
Device.SoftwareModules.NetworkConfig.Interfaces.1.Reference="Device.Logical.Interface.1."
Device.SoftwareModules.NetworkConfig.Interfaces.2.
Device.SoftwareModules.NetworkConfig.Interfaces.2.Name="Lan"
Device.SoftwareModules.NetworkConfig.Interfaces.2.Reference="Device.Logical.Interface.2."

root - * - [bus-cli] (-1)
 > exit
> !amx exit

root - * - [bus-cli] (0)
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-ls -f
NAME                                 STATE   AUTOSTART GROUPS IPV4 IPV6 UNPRIVILEGED 
edefaf21-2e07-5a48-bae5-85a516686e10 STOPPED 0         -      -    -    false        
root@lcm-vagrant-focal-rbus:/home/vagrant# ```
```

The procedure to stop the container can also be done using the rbuscli
##### rbuscli
```bash
rbuscli method_values "Device.SoftwareModules.ExecutionUnit.1.SetRequestedState()" RequestedState string "Idle"		
```

#### Start Container

##### ba-cli
```bash
Device.SoftwareModules.ExecutionUnit.1.SetRequestedState(RequestedState = "Active")
```
Now the container will start again and is possible to confirm the ExecutionUnit with "Active" state.

```bash
root - * - [bus-cli] (0)
 >Device.SoftwareModules.?&
Added subscription for Device.SoftwareModules.

root - * - [bus-cli] (0)
 > Device.SoftwareModules.ExecutionUnit.1.SetRequestedState(RequestedState = "Active")
Device.SoftwareModules.ExecutionUnit.1.SetRequestedState() returned
[
    2
]

[2024-07-05T00:33:03Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.1.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.1.Status = Idle -> Starting

[2024-07-05T00:33:04Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.1.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.1.Status = Starting -> Active

root - * - [bus-cli] (0)
 > Device.SoftwareModules.? 
Device.SoftwareModules.
Device.SoftwareModules.DeploymentUnitNumberOfEntries=1
Device.SoftwareModules.ExecEnvNumberOfEntries=1
Device.SoftwareModules.ExecutionUnitNumberOfEntries=1
Device.SoftwareModules.DeploymentUnit.1.
Device.SoftwareModules.DeploymentUnit.1.Alias="cpe-edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.DeploymentUnit.1.DUID="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.DeploymentUnit.1.Description="Unknown"
Device.SoftwareModules.DeploymentUnit.1.ExecutionEnvRef="Device.SoftwareModules.ExecEnv.1."
Device.SoftwareModules.DeploymentUnit.1.ExecutionUnitList=""
Device.SoftwareModules.DeploymentUnit.1.Installed=2024-07-05T00:05:56Z
Device.SoftwareModules.DeploymentUnit.1.LastUpdate=1970-01-01T00:00:00Z
Device.SoftwareModules.DeploymentUnit.1.ModuleVersion=""
Device.SoftwareModules.DeploymentUnit.1.Name="amd64/alpine"
Device.SoftwareModules.DeploymentUnit.1.Resolved=true
Device.SoftwareModules.DeploymentUnit.1.Status="Installed"
Device.SoftwareModules.DeploymentUnit.1.URL="https://index.docker.io/amd64/alpine:3.18"
Device.SoftwareModules.DeploymentUnit.1.UUID="10000000-0971-52b1-b70c-47ab6ce48128"
Device.SoftwareModules.DeploymentUnit.1.Vendor="Unknown"
Device.SoftwareModules.DeploymentUnit.1.VendorConfigList=""
Device.SoftwareModules.DeploymentUnit.1.VendorLogList=""
Device.SoftwareModules.DeploymentUnit.1.Version="3.18"
Device.SoftwareModules.ExecEnv.1.
Device.SoftwareModules.ExecEnv.1.ActiveExecutionUnits=""
Device.SoftwareModules.ExecEnv.1.Alias="cpe-generic"
Device.SoftwareModules.ExecEnv.1.AllocatedCPUPercent=100
Device.SoftwareModules.ExecEnv.1.AllocatedDiskSpace=5120
Device.SoftwareModules.ExecEnv.1.AllocatedMemory=-1
Device.SoftwareModules.ExecEnv.1.AvailableDiskSpace=4880
Device.SoftwareModules.ExecEnv.1.AvailableMemory=-1
Device.SoftwareModules.ExecEnv.1.CreatedAt=2024-07-04T23:50:43Z
Device.SoftwareModules.ExecEnv.1.CurrentRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Description=""
Device.SoftwareModules.ExecEnv.1.Enable=true
Device.SoftwareModules.ExecEnv.1.InitialExecutionUnitRunLevel=-1
Device.SoftwareModules.ExecEnv.1.InitialRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Name="generic"
Device.SoftwareModules.ExecEnv.1.ParentExecEnv=""
Device.SoftwareModules.ExecEnv.1.ProcessorRefList=""
Device.SoftwareModules.ExecEnv.1.RestartReason=""
Device.SoftwareModules.ExecEnv.1.Status="Up"
Device.SoftwareModules.ExecEnv.1.Type="lxc:5.0.2"
Device.SoftwareModules.ExecEnv.1.Vendor="Cthulhu"
Device.SoftwareModules.ExecEnv.1.Version="3.8.0"
Device.SoftwareModules.ExecutionUnit.1.
Device.SoftwareModules.ExecutionUnit.1.Alias="cpe-edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.1.AllocatedCPUPercent=-1
Device.SoftwareModules.ExecutionUnit.1.AllocatedDiskSpace=-1
Device.SoftwareModules.ExecutionUnit.1.AllocatedMemory=-1
Device.SoftwareModules.ExecutionUnit.1.AssociatedProcessList=""
Device.SoftwareModules.ExecutionUnit.1.AutoStart=true
Device.SoftwareModules.ExecutionUnit.1.AvailableDiskSpace=4880
Device.SoftwareModules.ExecutionUnit.1.AvailableMemory=-1
Device.SoftwareModules.ExecutionUnit.1.Description=""
Device.SoftwareModules.ExecutionUnit.1.DiskSpaceInUse=64
Device.SoftwareModules.ExecutionUnit.1.EUID="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.1.ExecEnvLabel="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.1.ExecutionEnvRef="generic"
Device.SoftwareModules.ExecutionUnit.1.ExecutionFaultCode="NoFault"
Device.SoftwareModules.ExecutionUnit.1.ExecutionFaultMessage=""
Device.SoftwareModules.ExecutionUnit.1.MemoryInUse=1692
Device.SoftwareModules.ExecutionUnit.1.Name="amd64/alpine"
Device.SoftwareModules.ExecutionUnit.1.References=""
Device.SoftwareModules.ExecutionUnit.1.RunLevel=0
Device.SoftwareModules.ExecutionUnit.1.Status="Active"
Device.SoftwareModules.ExecutionUnit.1.Vendor=""
Device.SoftwareModules.ExecutionUnit.1.VendorConfigList=""
Device.SoftwareModules.ExecutionUnit.1.VendorLogList=""
Device.SoftwareModules.ExecutionUnit.1.Version="3.18"
Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.
Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.X_PRPL-COM_DNSSDRefList=""
Device.SoftwareModules.NetworkConfig.
Device.SoftwareModules.NetworkConfig.DefaultBridge="br-lcm"
Device.SoftwareModules.NetworkConfig.DefaultFirewallChain="Device.Firewall.Chain.[Alias=="LCM"]."
Device.SoftwareModules.NetworkConfig.Interfaces.1.
Device.SoftwareModules.NetworkConfig.Interfaces.1.Name="Wan"
Device.SoftwareModules.NetworkConfig.Interfaces.1.Reference="Device.Logical.Interface.1."
Device.SoftwareModules.NetworkConfig.Interfaces.2.
Device.SoftwareModules.NetworkConfig.Interfaces.2.Name="Lan"
Device.SoftwareModules.NetworkConfig.Interfaces.2.Reference="Device.Logical.Interface.2."

root - * - [bus-cli] (0)
 > exit
> !amx exit

root - * - [bus-cli] (0)
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-ls -f
NAME                                 STATE   AUTOSTART GROUPS IPV4                     IPV6 UNPRIVILEGED 
edefaf21-2e07-5a48-bae5-85a516686e10 RUNNING 0         -      10.0.2.15, 192.168.200.8 -    false        
root@lcm-vagrant-focal-rbus:/home/vagrant# 
```

The procedure to start the container can also be done using the rbuscli.

##### rbuscli
```bash
rbuscli method_values "Device.SoftwareModules.ExecutionUnit.1.SetRequestedState()" RequestedState string "Active"
```

#### Uninstall Container

At this phase the Uninstall command will be used and is expected the container will be removed.
On previous examples the commands where using fixed indexes. However, the use of alias can also be used with the commands.

The next example will use the UUID=="10000000-0971-52b1-b70c-47ab6ce48128" that was used at the first phase on InstallDU()

##### ba-cli
```bash
Device.SoftwareModules.DeploymentUnit.[UUID=="10000000-0971-52b1-b70c-47ab6ce48128"].Uninstall(RetainData = "false")
```
Depending on the use cases, the option "RetainData" on Uninstall() can be used.

Confirmation:
```bash
root - * - [bus-cli] (0)
 > Device.SoftwareModules.DeploymentUnit.[UUID=="10000000-0971-52b1-b70c-47ab6ce48128"].Uninstall()

[2024-07-05T00:42:52Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.1.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.1.Status = Active -> Idle

[2024-07-05T00:42:52Z] Event dm:object-changed received from Device.SoftwareModules.DeploymentUnit.1.
<  dm:object-changed> Device.SoftwareModules.DeploymentUnit.1.Status = Installed -> Uninstalling

[2024-07-05T00:42:52Z] Event dm:object-removed received from Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.AccessInterfaces.
<  dm:object-removed> Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.AccessInterfaces.

[2024-07-05T00:42:52Z] Event dm:object-removed received from Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.PortForwarding.
<  dm:object-removed> Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.PortForwarding.

[2024-07-05T00:42:52Z] Event dm:object-removed received from Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.
<  dm:object-removed> Device.SoftwareModules.ExecutionUnit.1.NetworkConfig.

[2024-07-05T00:42:52Z] Event dm:object-removed received from Device.SoftwareModules.ExecutionUnit.1.HostObject.
<  dm:object-removed> Device.SoftwareModules.ExecutionUnit.1.HostObject.

[2024-07-05T00:42:52Z] Event dm:object-removed received from Device.SoftwareModules.ExecutionUnit.1.EnvVariable.
<  dm:object-removed> Device.SoftwareModules.ExecutionUnit.1.EnvVariable.

[2024-07-05T00:42:52Z] Event dm:object-removed received from Device.SoftwareModules.ExecutionUnit.1.Extensions.
<  dm:object-removed> Device.SoftwareModules.ExecutionUnit.1.Extensions.

[2024-07-05T00:42:52Z] Event dm:instance-removed received from Device.SoftwareModules.ExecutionUnit.
<dm:instance-removed> Device.SoftwareModules.ExecutionUnit.1.

[2024-07-05T00:42:52Z] Event dm:object-changed received from Device.SoftwareModules.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnitNumberOfEntries = 1 -> 0
Device.SoftwareModules.DeploymentUnit.1.Uninstall() returned
[
    <NULL>
]

[2024-07-05T00:42:52Z] Event dm:object-changed received from Device.SoftwareModules.DeploymentUnit.1.
<  dm:object-changed> Device.SoftwareModules.DeploymentUnit.1.Description = Unknown -> 
<                   > Device.SoftwareModules.DeploymentUnit.1.Vendor = Unknown -> 

[2024-07-05T00:42:52Z] Event DUStateChange! received from Device.SoftwareModules.DeploymentUnit.1.
{
    data = {
        CurrentState = "Uninstalled",
        ExecutionEnvRef = "Device.SoftwareModules.ExecEnv.1.",
        ExecutionUnitRefList = "",
        Fault.FaultCode = 0,
        Fault.FaultString = "",
        OperationPerformed = "Uninstall",
        Resolved = true,
        UUID = "10000000-0971-52b1-b70c-47ab6ce48128",
        Version = "3.18"
    },
    eobject = "Device.SoftwareModules.DeploymentUnit.[cpe-edefaf21-2e07-5a48-bae5-85a516686e10].",
    notification = "DUStateChange!",
    object = "Device.SoftwareModules.DeploymentUnit.cpe-edefaf21-2e07-5a48-bae5-85a516686e10.",
    path = "Device.SoftwareModules.DeploymentUnit.1."
}

[2024-07-05T00:42:52Z] Event dm:instance-removed received from Device.SoftwareModules.DeploymentUnit.
<dm:instance-removed> Device.SoftwareModules.DeploymentUnit.1.

[2024-07-05T00:42:52Z] Event dm:object-changed received from Device.SoftwareModules.
<  dm:object-changed> Device.SoftwareModules.DeploymentUnitNumberOfEntries = 1 -> 0

root - * - [bus-cli] (0)
 > Device.SoftwareModules.?
Device.SoftwareModules.
Device.SoftwareModules.DeploymentUnitNumberOfEntries=0
Device.SoftwareModules.ExecEnvNumberOfEntries=1
Device.SoftwareModules.ExecutionUnitNumberOfEntries=0
Device.SoftwareModules.ExecEnv.1.
Device.SoftwareModules.ExecEnv.1.ActiveExecutionUnits=""
Device.SoftwareModules.ExecEnv.1.Alias="cpe-generic"
Device.SoftwareModules.ExecEnv.1.AllocatedCPUPercent=100
Device.SoftwareModules.ExecEnv.1.AllocatedDiskSpace=5120
Device.SoftwareModules.ExecEnv.1.AllocatedMemory=-1
Device.SoftwareModules.ExecEnv.1.AvailableDiskSpace=4916
Device.SoftwareModules.ExecEnv.1.AvailableMemory=-1
Device.SoftwareModules.ExecEnv.1.CreatedAt=2024-07-04T23:50:43Z
Device.SoftwareModules.ExecEnv.1.CurrentRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Description=""
Device.SoftwareModules.ExecEnv.1.Enable=true
Device.SoftwareModules.ExecEnv.1.InitialExecutionUnitRunLevel=-1
Device.SoftwareModules.ExecEnv.1.InitialRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Name="generic"
Device.SoftwareModules.ExecEnv.1.ParentExecEnv=""
Device.SoftwareModules.ExecEnv.1.ProcessorRefList=""
Device.SoftwareModules.ExecEnv.1.RestartReason=""
Device.SoftwareModules.ExecEnv.1.Status="Up"
Device.SoftwareModules.ExecEnv.1.Type="lxc:5.0.2"
Device.SoftwareModules.ExecEnv.1.Vendor="Cthulhu"
Device.SoftwareModules.ExecEnv.1.Version="3.8.0"
Device.SoftwareModules.NetworkConfig.
Device.SoftwareModules.NetworkConfig.DefaultBridge="br-lcm"
Device.SoftwareModules.NetworkConfig.DefaultFirewallChain="Device.Firewall.Chain.[Alias=="LCM"]."
Device.SoftwareModules.NetworkConfig.Interfaces.1.
Device.SoftwareModules.NetworkConfig.Interfaces.1.Name="Wan"
Device.SoftwareModules.NetworkConfig.Interfaces.1.Reference="Device.Logical.Interface.1."
Device.SoftwareModules.NetworkConfig.Interfaces.2.
Device.SoftwareModules.NetworkConfig.Interfaces.2.Name="Lan"
Device.SoftwareModules.NetworkConfig.Interfaces.2.Reference="Device.Logical.Interface.2."

root - * - [bus-cli] (0)
 > 
root - * - [bus-cli] (0)
 > exit
> !amx exit
root - * - [bus-cli] (0)
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-ls -f
root@lcm-vagrant-focal-rbus:/home/vagrant# 
```


##### rbuscli
```bash
rbuscli method_noargs "Device.SoftwareModules.DeploymentUnit.1.Uninstall()"
```

#### Update the Container

Prpl LCM also supports the Update() command. This command can be used to upgrade the alpine container to a different version.
In this case, we have installed previously the alpine container version 3.18.

```bash
root - * - [bus-cli] (-1)
 > Device.SoftwareModules.DeploymentUnit.1.Version?
Device.SoftwareModules.DeploymentUnit.1.Version="3.18"
root - * - [bus-cli] (-1)
 > Device.SoftwareModules.ExecutionUnit.1.Version?
Device.SoftwareModules.ExecutionUnit.1.Version="3.18"
root - * - [bus-cli] (0)
 > exit
> !amx exit
root - * - [bus-cli] (0)
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-ls -f
NAME                                 STATE   AUTOSTART GROUPS IPV4                     IPV6 UNPRIVILEGED
edefaf21-2e07-5a48-bae5-85a516686e10 RUNNING 0         -      10.0.2.15, 192.168.200.8 -    false        
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-attach edefaf21-2e07-5a48-bae5-85a516686e10
/ # cat /etc/
alpine-release    conf.d/           group             init.d/           logrotate.d/      modules-load.d/   network/          os-release        profile           secfixes.d/       shadow            ssl1.1/           udhcpd.conf
apk/              crontabs/         hostname          inittab           modprobe.d/       motd              nsswitch.conf     passwd            profile.d/        securetty         shells            sysctl.conf
busybox-paths.d/  fstab             hosts             issue             modules           mtab              opt/              periodic/         protocols         services          ssl/              sysctl.d/
/ # cat /etc/alpine-release
3.18.7
/ # 
```

The next step is upgrade the container to version

##### ba-cli
```bash
Device.SoftwareModules.DeploymentUnit.[UUID=="10000000-0971-52b1-b70c-47ab6ce48128"].Update(URL = "https://index.docker.io/amd64/alpine:3.20.1", AutoStart = "true")
```

After the update is complete, the container will start automatically with the new version:
```bash
root - * - [bus-cli] (0)
 > Device.SoftwareModules.DeploymentUnit.[UUID=="10000000-0971-52b1-b70c-47ab6ce48128"].Update(URL = "https://index.docker.io/amd64/alpine:3.20.1", AutoStart = "true")

[2024-07-05T01:09:31Z] Event dm:object-changed received from Device.SoftwareModules.DeploymentUnit.5.
<  dm:object-changed> Device.SoftwareModules.DeploymentUnit.5.URL = https://index.docker.io/amd64/alpine:3.18 -> https://index.docker.io/amd64/alpine:3.20.1
<                   > Device.SoftwareModules.DeploymentUnit.5.Version = 3.18 -> 3.20.1
<                   > Device.SoftwareModules.DeploymentUnit.5.Description = Unknown -> 
<                   > Device.SoftwareModules.DeploymentUnit.5.Vendor = Unknown -> 

[2024-07-05T01:09:42Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.5.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.5.Status = Active -> Idle

[2024-07-05T01:09:42Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.5.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.5.Version = 3.18 -> 3.20.1
Device.SoftwareModules.DeploymentUnit.5.Update() returned
[
    <NULL>
]

[2024-07-05T01:09:42Z] Event dm:object-changed received from Device.SoftwareModules.DeploymentUnit.5.
<  dm:object-changed> Device.SoftwareModules.DeploymentUnit.5.LastUpdate = 1970-01-01T00:00:00Z -> 2024-07-05T01:09:42Z

[2024-07-05T01:09:42Z] Event DUStateChange! received from Device.SoftwareModules.DeploymentUnit.5.
{
    data = {
        CurrentState = "",
        ExecutionEnvRef = "Device.SoftwareModules.ExecEnv.1.",
        ExecutionUnitRefList = "Device.SoftwareModules.ExecutionUnit.5",
        Fault.FaultCode = 0,
        Fault.FaultString = "",
        OperationPerformed = "Update",
        Resolved = true,
        UUID = "10000000-0971-52b1-b70c-47ab6ce48128",
        Version = "3.20.1"
    },
    eobject = "Device.SoftwareModules.DeploymentUnit.[cpe-edefaf21-2e07-5a48-bae5-85a516686e10].",
    notification = "DUStateChange!",
    object = "Device.SoftwareModules.DeploymentUnit.cpe-edefaf21-2e07-5a48-bae5-85a516686e10.",
    path = "Device.SoftwareModules.DeploymentUnit.5."
}

[2024-07-05T01:09:42Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.5.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.5.Status = Idle -> Starting

[2024-07-05T01:09:44Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.5.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.5.Status = Starting -> Active

root - * - [bus-cli] (0)
 > Device.SoftwareModules.? 
Device.SoftwareModules.
Device.SoftwareModules.DeploymentUnitNumberOfEntries=1
Device.SoftwareModules.ExecEnvNumberOfEntries=1
Device.SoftwareModules.ExecutionUnitNumberOfEntries=1
Device.SoftwareModules.DeploymentUnit.5.
Device.SoftwareModules.DeploymentUnit.5.Alias="cpe-edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.DeploymentUnit.5.DUID="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.DeploymentUnit.5.Description=""
Device.SoftwareModules.DeploymentUnit.5.ExecutionEnvRef="Device.SoftwareModules.ExecEnv.1."
Device.SoftwareModules.DeploymentUnit.5.ExecutionUnitList=""
Device.SoftwareModules.DeploymentUnit.5.Installed=2024-07-05T01:09:02Z
Device.SoftwareModules.DeploymentUnit.5.LastUpdate=2024-07-05T01:09:42Z
Device.SoftwareModules.DeploymentUnit.5.ModuleVersion=""
Device.SoftwareModules.DeploymentUnit.5.Name="amd64/alpine"
Device.SoftwareModules.DeploymentUnit.5.Resolved=true
Device.SoftwareModules.DeploymentUnit.5.Status="Installed"
Device.SoftwareModules.DeploymentUnit.5.URL="https://index.docker.io/amd64/alpine:3.20.1"
Device.SoftwareModules.DeploymentUnit.5.UUID="10000000-0971-52b1-b70c-47ab6ce48128"
Device.SoftwareModules.DeploymentUnit.5.Vendor=""
Device.SoftwareModules.DeploymentUnit.5.VendorConfigList=""
Device.SoftwareModules.DeploymentUnit.5.VendorLogList=""
Device.SoftwareModules.DeploymentUnit.5.Version="3.20.1"
Device.SoftwareModules.ExecEnv.1.
Device.SoftwareModules.ExecEnv.1.ActiveExecutionUnits=""
Device.SoftwareModules.ExecEnv.1.Alias="cpe-generic"
Device.SoftwareModules.ExecEnv.1.AllocatedCPUPercent=100
Device.SoftwareModules.ExecEnv.1.AllocatedDiskSpace=5120
Device.SoftwareModules.ExecEnv.1.AllocatedMemory=-1
Device.SoftwareModules.ExecEnv.1.AvailableDiskSpace=4896
Device.SoftwareModules.ExecEnv.1.AvailableMemory=-1
Device.SoftwareModules.ExecEnv.1.CreatedAt=2024-07-04T23:50:43Z
Device.SoftwareModules.ExecEnv.1.CurrentRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Description=""
Device.SoftwareModules.ExecEnv.1.Enable=true
Device.SoftwareModules.ExecEnv.1.InitialExecutionUnitRunLevel=-1
Device.SoftwareModules.ExecEnv.1.InitialRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Name="generic"
Device.SoftwareModules.ExecEnv.1.ParentExecEnv=""
Device.SoftwareModules.ExecEnv.1.ProcessorRefList=""
Device.SoftwareModules.ExecEnv.1.RestartReason=""
Device.SoftwareModules.ExecEnv.1.Status="Up"
Device.SoftwareModules.ExecEnv.1.Type="lxc:5.0.2"
Device.SoftwareModules.ExecEnv.1.Vendor="Cthulhu"
Device.SoftwareModules.ExecEnv.1.Version="3.8.0"
Device.SoftwareModules.ExecutionUnit.5.
Device.SoftwareModules.ExecutionUnit.5.Alias="cpe-edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.5.AllocatedCPUPercent=-1
Device.SoftwareModules.ExecutionUnit.5.AllocatedDiskSpace=-1
Device.SoftwareModules.ExecutionUnit.5.AllocatedMemory=-1
Device.SoftwareModules.ExecutionUnit.5.AssociatedProcessList=""
Device.SoftwareModules.ExecutionUnit.5.AutoStart=true
Device.SoftwareModules.ExecutionUnit.5.AvailableDiskSpace=4896
Device.SoftwareModules.ExecutionUnit.5.AvailableMemory=-1
Device.SoftwareModules.ExecutionUnit.5.Description=""
Device.SoftwareModules.ExecutionUnit.5.DiskSpaceInUse=48
Device.SoftwareModules.ExecutionUnit.5.EUID="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.5.ExecEnvLabel="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.5.ExecutionEnvRef="generic"
Device.SoftwareModules.ExecutionUnit.5.ExecutionFaultCode="NoFault"
Device.SoftwareModules.ExecutionUnit.5.ExecutionFaultMessage=""
Device.SoftwareModules.ExecutionUnit.5.MemoryInUse=6192
Device.SoftwareModules.ExecutionUnit.5.Name="amd64/alpine"
Device.SoftwareModules.ExecutionUnit.5.References=""
Device.SoftwareModules.ExecutionUnit.5.RunLevel=0
Device.SoftwareModules.ExecutionUnit.5.Status="Active"
Device.SoftwareModules.ExecutionUnit.5.Vendor=""
Device.SoftwareModules.ExecutionUnit.5.VendorConfigList=""
Device.SoftwareModules.ExecutionUnit.5.VendorLogList=""
Device.SoftwareModules.ExecutionUnit.5.Version="3.20.1"
Device.SoftwareModules.ExecutionUnit.5.NetworkConfig.
Device.SoftwareModules.ExecutionUnit.5.NetworkConfig.X_PRPL-COM_DNSSDRefList=""
Device.SoftwareModules.NetworkConfig.
Device.SoftwareModules.NetworkConfig.DefaultBridge="br-lcm"
Device.SoftwareModules.NetworkConfig.DefaultFirewallChain="Device.Firewall.Chain.[Alias=="LCM"]."
Device.SoftwareModules.NetworkConfig.Interfaces.1.
Device.SoftwareModules.NetworkConfig.Interfaces.1.Name="Wan"
Device.SoftwareModules.NetworkConfig.Interfaces.1.Reference="Device.Logical.Interface.1."
Device.SoftwareModules.NetworkConfig.Interfaces.2.
Device.SoftwareModules.NetworkConfig.Interfaces.2.Name="Lan"
Device.SoftwareModules.NetworkConfig.Interfaces.2.Reference="Device.Logical.Interface.2."

root - * - [bus-cli] (0)
 > 
root - * - [bus-cli] (0)
 > exit
> !amx exit

root - * - [bus-cli] (0)
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-ls -f
NAME                                 STATE   AUTOSTART GROUPS IPV4                     IPV6 UNPRIVILEGED 
edefaf21-2e07-5a48-bae5-85a516686e10 RUNNING 0         -      10.0.2.15, 192.168.200.8 -    false        
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-attach edefaf21-2e07-5a48-bae5-85a516686e10
/ # cat /etc/alpine-release 
3.20.1
/ # 
```

##### rbuscli
```bash
rbuscli method_values "Device.SoftwareModules.DeploymentUnit.1.Update()" URL string "https://index.docker.io/amd64/alpine:3.20.1" UUID string "10000000-0971-52b1-b70c-47ab6ce48128" AutoStart string "true"
```


#### Resources constrainst

Prpl LCM also supports the capability to define limits for the resources that are being used by the containers.
Depending on the use case, defining the resource restrictions on prpl LCM will ensure the containers will not consume all memory, disk, and CPU.

This is part of the BBF standard and can be defined on ExecEnv and ExecUnit:
Execution Unit
* https://usp-data-models.broadband-forum.org/tr-181-2-17-0-usp.html#D.Device:2.Device.SoftwareModules.ExecutionUnit.AllocatedDiskSpace
* https://usp-data-models.broadband-forum.org/tr-181-2-17-0-usp.html#D.Device:2.Device.SoftwareModules.ExecutionUnit.AllocatedMemory
* https://usp-data-models.broadband-forum.org/tr-181-2-17-0-usp.html#D.Device:2.Device.SoftwareModules.ExecutionUnit.AllocatedCPUPercent

Execution Environment:
* https://usp-data-models.broadband-forum.org/tr-181-2-17-0-usp.html#D.Device:2.Device.SoftwareModules.ExecEnv.AllocatedDiskSpace
* https://usp-data-models.broadband-forum.org/tr-181-2-17-0-usp.html#D.Device:2.Device.SoftwareModules.ExecEnv.AllocatedMemory
* https://usp-data-models.broadband-forum.org/tr-181-2-17-0-usp.html#D.Device:2.Device.SoftwareModules.ExecEnv.AllocatedCPUPercent


Prpl LCM supports two levels of restrictions:
Execution Environment - Defines the maximum resources all agents will have
Execution Unit - Defines the maximum resources a container  will have

This is already supported by prpl LCM. If you want to test you can use the following:

##### ba-cli:

Define the Constraints for EE:
```bash
Device.SoftwareModules.ExecEnv.1.ModifyConstraints(AllocatedCPUPercent = 50, AllocatedDiskSpace = 400000, AllocatedMemory = 300000)
```

Restart EE:
```bash
Device.SoftwareModules.ExecEnv.1.Restart(RestartReason="Restart_Test")
```

Then on InstallDU() or Update() use the following extra options:
```bash
AllocatedCPUPercent =10, AllocatedDiskSpace=60000, AllocatedMemory=40000
```

InstallDU examples with restrictions:
```bash
Device.SoftwareModules.InstallDU(URL = "https://registry.gitlab.com/prpl-foundation/prplos/prplos/prplos/lcm-test-x86-64:prplos-v1", UUID = "40000000-0971-52b1-b70c-47ab6cd48239", ExecutionEnvRef = "Device.SoftwareModules.ExecEnv.1.", AutoStart = "true", AllocatedCPUPercent =10, AllocatedDiskSpace=60000, AllocatedMemory=40000)
```

#### HostObject - Enable sharing of sockets, files, and directories from the host to the container

The HostObject feature implements the standard TR181 HostObject definition, offering a powerful means of sharing host resources with containers. This functionality allows containers to access and utilize various host resources seamlessly. Whether it's accessing shared files, using host hardware resources (such as devices), or sockets allowing communicating with other services running on the host, the HostObject feature streamlines the interaction between containers and the underlying host system, enhancing the overall flexibility and efficiency of the container ecosystem.

Defined on BBF standard:
* https://usp-data-models.broadband-forum.org/tr-181-2-17-0-usp.html#D.Device:2.Device.SoftwareModules.ExecutionUnit.HostObject.

This configuration can be passed on Install() and Update() commands.
Below and example using the alpine container. For this example a folder on vagrant was created with the name "cpe_folder_hostobject". This folder will then be mapped with the container folder "/home"

```
root@lcm-vagrant-focal-rbus:/home/vagrant# mkdir cpe_folder_hostobject
root@lcm-vagrant-focal-rbus:/home/vagrant# cd cpe_folder_hostobject/
root@lcm-vagrant-focal-rbus:/home/vagrant/cpe_folder_hostobject# ls
root@lcm-vagrant-focal-rbus:/home/vagrant/cpe_folder_hostobject#
```

##### ba-cli
```bash
Device.SoftwareModules.DeploymentUnit.[UUID=="10000000-0971-52b1-b70c-47ab6ce48128"].Update(URL = "https://index.docker.io/amd64/alpine:3.20.1", AutoStart = "true", HostObject = [{Destination = "/home", Options = "type=mount,bind,exec", Source = "/home/vagrant/cpe_folder_hostobject"}])
```

As you can see, the HostObject was create using the Update() command:

```bash
root - * - [bus-cli] (0)
 > Device.SoftwareModules.DeploymentUnit.[UUID=="10000000-0971-52b1-b70c-47ab6ce48128"].Update(URL = "https://index.docker.io/amd64/alpine:3.20.1", AutoStart = "true", HostObject = [{Destination = "/home", Options = "type=mount,bind,exec", Source = "/home/vagrant/cpe_folder_hostobject"}])

[2024-07-05T01:42:28Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.6.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.6.Status = Active -> Idle
Device.SoftwareModules.DeploymentUnit.6.Update() returned
[
    <NULL>
]

[2024-07-05T01:42:28Z] Event dm:instance-added received from Device.SoftwareModules.ExecutionUnit.6.HostObject.
<  dm:instance-added> Device.SoftwareModules.ExecutionUnit.6.HostObject.1.Options = type=mount,bind,exec
<                   > Device.SoftwareModules.ExecutionUnit.6.HostObject.1.Alias = cpe-HostObject-1
<                   > Device.SoftwareModules.ExecutionUnit.6.HostObject.1.Source = /home/vagrant/cpe_folder_hostobject
<                   > Device.SoftwareModules.ExecutionUnit.6.HostObject.1.Destination = /home

[2024-07-05T01:42:28Z] Event dm:object-changed received from Device.SoftwareModules.DeploymentUnit.6.
<  dm:object-changed> Device.SoftwareModules.DeploymentUnit.6.LastUpdate = 2024-07-05T01:19:49Z -> 2024-07-05T01:42:28Z

[2024-07-05T01:42:28Z] Event DUStateChange! received from Device.SoftwareModules.DeploymentUnit.6.
{
    data = {
        CurrentState = "",
        ExecutionEnvRef = "Device.SoftwareModules.ExecEnv.1.",
        ExecutionUnitRefList = "Device.SoftwareModules.ExecutionUnit.6",
        Fault.FaultCode = 0,
        Fault.FaultString = "",
        OperationPerformed = "Update",
        Resolved = true,
        UUID = "10000000-0971-52b1-b70c-47ab6ce48128",
        Version = "3.20.1"
    },
    eobject = "Device.SoftwareModules.DeploymentUnit.[cpe-edefaf21-2e07-5a48-bae5-85a516686e10].",
    notification = "DUStateChange!",
    object = "Device.SoftwareModules.DeploymentUnit.cpe-edefaf21-2e07-5a48-bae5-85a516686e10.",
    path = "Device.SoftwareModules.DeploymentUnit.6."
}

[2024-07-05T01:42:28Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.6.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.6.Status = Idle -> Starting

[2024-07-05T01:42:30Z] Event dm:object-changed received from Device.SoftwareModules.ExecutionUnit.6.
<  dm:object-changed> Device.SoftwareModules.ExecutionUnit.6.Status = Starting -> Active

root - * - [bus-cli] (0)
 > Device.SoftwareModules.? Device.SoftwareModules.
Device.SoftwareModules.DeploymentUnitNumberOfEntries=1
Device.SoftwareModules.ExecEnvNumberOfEntries=1
Device.SoftwareModules.ExecutionUnitNumberOfEntries=1
Device.SoftwareModules.DeploymentUnit.6.
Device.SoftwareModules.DeploymentUnit.6.Alias="cpe-edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.DeploymentUnit.6.DUID="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.DeploymentUnit.6.Description=""
Device.SoftwareModules.DeploymentUnit.6.ExecutionEnvRef="Device.SoftwareModules.ExecEnv.1."
Device.SoftwareModules.DeploymentUnit.6.ExecutionUnitList=""
Device.SoftwareModules.DeploymentUnit.6.Installed=2024-07-05T01:18:46Z
Device.SoftwareModules.DeploymentUnit.6.LastUpdate=2024-07-05T01:42:28Z
Device.SoftwareModules.DeploymentUnit.6.ModuleVersion=""
Device.SoftwareModules.DeploymentUnit.6.Name="amd64/alpine"
Device.SoftwareModules.DeploymentUnit.6.Resolved=true
Device.SoftwareModules.DeploymentUnit.6.Status="Installed"
Device.SoftwareModules.DeploymentUnit.6.URL="https://index.docker.io/amd64/alpine:3.20.1"
Device.SoftwareModules.DeploymentUnit.6.UUID="10000000-0971-52b1-b70c-47ab6ce48128"
Device.SoftwareModules.DeploymentUnit.6.Vendor=""
Device.SoftwareModules.DeploymentUnit.6.VendorConfigList=""
Device.SoftwareModules.DeploymentUnit.6.VendorLogList=""
Device.SoftwareModules.DeploymentUnit.6.Version="3.20.1"
Device.SoftwareModules.ExecEnv.1.
Device.SoftwareModules.ExecEnv.1.ActiveExecutionUnits=""
Device.SoftwareModules.ExecEnv.1.Alias="cpe-generic"
Device.SoftwareModules.ExecEnv.1.AllocatedCPUPercent=100
Device.SoftwareModules.ExecEnv.1.AllocatedDiskSpace=5120
Device.SoftwareModules.ExecEnv.1.AllocatedMemory=-1
Device.SoftwareModules.ExecEnv.1.AvailableDiskSpace=4896
Device.SoftwareModules.ExecEnv.1.AvailableMemory=-1
Device.SoftwareModules.ExecEnv.1.CreatedAt=2024-07-04T23:50:43Z
Device.SoftwareModules.ExecEnv.1.CurrentRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Description=""
Device.SoftwareModules.ExecEnv.1.Enable=true
Device.SoftwareModules.ExecEnv.1.InitialExecutionUnitRunLevel=-1
Device.SoftwareModules.ExecEnv.1.InitialRunLevel=-1
Device.SoftwareModules.ExecEnv.1.Name="generic"
Device.SoftwareModules.ExecEnv.1.ParentExecEnv=""
Device.SoftwareModules.ExecEnv.1.ProcessorRefList=""
Device.SoftwareModules.ExecEnv.1.RestartReason=""
Device.SoftwareModules.ExecEnv.1.Status="Up"
Device.SoftwareModules.ExecEnv.1.Type="lxc:5.0.2"
Device.SoftwareModules.ExecEnv.1.Vendor="Cthulhu"
Device.SoftwareModules.ExecEnv.1.Version="3.8.0"
Device.SoftwareModules.ExecutionUnit.6.
Device.SoftwareModules.ExecutionUnit.6.Alias="cpe-edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.6.AllocatedCPUPercent=-1
Device.SoftwareModules.ExecutionUnit.6.AllocatedDiskSpace=-1
Device.SoftwareModules.ExecutionUnit.6.AllocatedMemory=-1
Device.SoftwareModules.ExecutionUnit.6.AssociatedProcessList=""
Device.SoftwareModules.ExecutionUnit.6.AutoStart=true
Device.SoftwareModules.ExecutionUnit.6.AvailableDiskSpace=4896
Device.SoftwareModules.ExecutionUnit.6.AvailableMemory=-1
Device.SoftwareModules.ExecutionUnit.6.Description=""
Device.SoftwareModules.ExecutionUnit.6.DiskSpaceInUse=48
Device.SoftwareModules.ExecutionUnit.6.EUID="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.6.ExecEnvLabel="edefaf21-2e07-5a48-bae5-85a516686e10"
Device.SoftwareModules.ExecutionUnit.6.ExecutionEnvRef="generic"
Device.SoftwareModules.ExecutionUnit.6.ExecutionFaultCode="NoFault"
Device.SoftwareModules.ExecutionUnit.6.ExecutionFaultMessage=""
Device.SoftwareModules.ExecutionUnit.6.MemoryInUse=7812
Device.SoftwareModules.ExecutionUnit.6.Name="amd64/alpine"
Device.SoftwareModules.ExecutionUnit.6.References=""
Device.SoftwareModules.ExecutionUnit.6.RunLevel=0
Device.SoftwareModules.ExecutionUnit.6.Status="Active"
Device.SoftwareModules.ExecutionUnit.6.Vendor=""
Device.SoftwareModules.ExecutionUnit.6.VendorConfigList=""
Device.SoftwareModules.ExecutionUnit.6.VendorLogList=""
Device.SoftwareModules.ExecutionUnit.6.Version="3.20.1"
Device.SoftwareModules.ExecutionUnit.6.HostObject.1.
Device.SoftwareModules.ExecutionUnit.6.HostObject.1.Alias="cpe-HostObject-1"
Device.SoftwareModules.ExecutionUnit.6.HostObject.1.Destination="/home"
Device.SoftwareModules.ExecutionUnit.6.HostObject.1.Options="type=mount,bind,exec"
Device.SoftwareModules.ExecutionUnit.6.HostObject.1.Source="/home/vagrant/cpe_folder_hostobject"
Device.SoftwareModules.ExecutionUnit.6.NetworkConfig.
Device.SoftwareModules.ExecutionUnit.6.NetworkConfig.X_PRPL-COM_DNSSDRefList=""
Device.SoftwareModules.NetworkConfig.
Device.SoftwareModules.NetworkConfig.DefaultBridge="br-lcm"
Device.SoftwareModules.NetworkConfig.DefaultFirewallChain="Device.Firewall.Chain.[Alias=="LCM"]."
Device.SoftwareModules.NetworkConfig.Interfaces.1.
Device.SoftwareModules.NetworkConfig.Interfaces.1.Name="Wan"
Device.SoftwareModules.NetworkConfig.Interfaces.1.Reference="Device.Logical.Interface.1."
Device.SoftwareModules.NetworkConfig.Interfaces.2.
Device.SoftwareModules.NetworkConfig.Interfaces.2.Name="Lan"
Device.SoftwareModules.NetworkConfig.Interfaces.2.Reference="Device.Logical.Interface.2."

root - * - [bus-cli] (0)
 > 

root - * - [bus-cli] (0)
 > exit
> !amx exit

root - * - [bus-cli] (0)
```
Now the "/home" folder inside the container has access to the folder in the vagrant machine "/home/vagrant/cpe_folder_hostobject".
For this test I'll create a file on vagrant machine just for testing.

```bash
root@lcm-vagrant-focal-rbus:/home/vagrant# cd cpe_folder_hostobject/
root@lcm-vagrant-focal-rbus:/home/vagrant/cpe_folder_hostobject# ls
root@lcm-vagrant-focal-rbus:/home/vagrant/cpe_folder_hostobject# ls
root@lcm-vagrant-focal-rbus:/home/vagrant/cpe_folder_hostobject# touch test_file
root@lcm-vagrant-focal-rbus:/home/vagrant/cpe_folder_hostobject# ls -altr
total 8
drwxr-xr-x 16 vagrant vagrant 4096 Jul  5 01:38 ..
-rw-r--r--  1 root    root       0 Jul  5 01:44 test_file
drwxr-xr-x  2 root    root    4096 Jul  5 01:44 .
root@lcm-vagrant-focal-rbus:/home/vagrant/cpe_folder_hostobject# 
```
Then, accesing to the "/home" folder inside the container the "test_file" created on vagrant machine will be accessible inside the container.
```bash
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-ls -f
NAME                                 STATE   AUTOSTART GROUPS IPV4                     IPV6 UNPRIVILEGED 
edefaf21-2e07-5a48-bae5-85a516686e10 RUNNING 0         -      10.0.2.15, 192.168.200.8 -    false        
root@lcm-vagrant-focal-rbus:/home/vagrant# lxc-attach edefaf21-2e07-5a48-bae5-85a516686e10
/ # cd home/
/home # ls -altr
total 12
drwxr-xr-x    1 root     root          4096 Jul  5 01:42 ..
-rw-r--r--    1 root     root             0 Jul  5 01:44 test_file
drwxr-xr-x    2 root     root          4096 Jul  5 01:44 .
/home # 
```
Example of HostObject for creating a socket for rbus:
```bash
{Destination = "/tmp/rtrouted", Options = "type=mount,bind,exec", Source = "/tmp/rtrouted"}
```
Example of HostObject enabling sharing of a device from the host to the container.
```
HostObject = [{ "Source"= "", "Destination" = "/dev/host_console", Options = "type=device,devicetype=c,major=5,minor=1,access=rwm,create=1"}, {"Source"= "", "Destination" = "/dev/ttyS0", Options = " type=device,devicetype=c,major=4,minor=64,access=rwm,create=1"}]
```

Example of HostObjects on InstallDU()
```bash
Device.SoftwareModules.InstallDU(URL = "https://index.docker.io/amd64/alpine:3.18", UUID = "10000000-0971-52b1-b70c-47ab6ce48128", ExecutionEnvRef = "Device.SoftwareModules.ExecEnv.1.", AutoStart = "true", HostObject = [{Destination = "/home", Options = "type=mount,bind,exec", Source = "/home/vagrant/cpe_folder_hostobject"}])
```
Note: The rbuscli utility does not support adding object argument types within commands. While the rbus API itself supports this feature, it is not currently available in rbuscli

## 2. Future Roadmap features
* Unprivileged Containers
* ApplicationData - Device.SoftwareModules.ExecEnv.{i}.ApplicationData.{i}.
* AutoRestart configuration support in InstallDU (partially available)
* ExecutionUnit Extensions - Device.SoftwareModules.ExecutionUnit.{i}.Extensions
* cgroup v2 support
* crun and oci bundle support (ConsultRed contributions)

## 3. Next Steps
- Add obuspa and support for DUStataChange! event and also support async commands (InstallDU, Update(), Uninstall(), etc)
* Add documentation for NetworkConfig
    * The NetworkConfig feature enables the efficient management of a container's network configuration
    * Datamodel - Device.SoftwareModules.NetworkConfig.Interfaces.{i}
* Add documentation for the "Service advertisement through DNS.SD"
* Add documentation to help understand how the pre-embbeded / onboarding feature (currently available) can be used
* Improve documentation available on prpl foundation wiki - https://prpl-foundation.gitlab.io/prplos/feeds/feed-prpl/SoftwareModules..html
